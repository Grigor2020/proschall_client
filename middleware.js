const request = require('request');
var statics = require('./static');
var statisMethods = require('./model/staticMethods');
var translations = require('./translate');

const checkUser = function (req, res, next) {
    var cookie = req.cookies['void'];
    if(typeof cookie === 'undefined'){
        next();
    }else{
        const formData = {
            "token" :cookie
        };
        request.post(
            {
                url: statics.API_URL+'/auth/check-user',
                headers: {
                    'Accept': 'application/json',
                    'authorization' : statics.API_AUTH
                },
                form: formData
            }, function (err, httpResponse, body) {
                var jsonBody = JSON.parse(body);
                if(!jsonBody.error){
                    // console.log('jsonBody',jsonBody.notes)
                    req.siteData.userInfo = jsonBody.user;
                    req.siteData.notifications = jsonBody.notes
                    // req.siteData.notifications = statisMethods.byNote(jsonBody.notes,req.siteData.lang);
                    // console.log('req.siteData.notifications',req.siteData.notifications)
                    req.siteData.unreadNotes = jsonBody.unreadCount;
                    req.siteData.showMsg = jsonBody.showMsg;
                }
                next();
            }
        )
    }
}

// const getTopLeagues = function (req, res, next) {
//     request.post(
//         {
//             url: statics.API_URL+'/football/leagues/topLeagues',
//             headers: {
//                 'Accept': 'application/json',
//                 'authorization' : statics.API_AUTH
//             }
//         }, function (err, httpResponse, body) {
//             var jsonBody = JSON.parse(body);
//             if(!jsonBody.error){
//                 req.topLeagues = jsonBody.data;
//             }
//             next();
//         }
//     )
// }

const getAllLanguages = function (req, res, next) {
    const options = {
        url: `${statics.API_URL}/language/all`,
        headers: {
            'authorization': statics.API_AUTH
        }
    };
    request(options, function (error, response, body) {
        const info = JSON.parse(body);
        if(!info.error){
            req.siteData.allLanguages = info.data;
        }
        next();
    });
}


const setlanguage = function (req, res, next) {
    var cookieLang = req.cookies['lang'];
    if(typeof cookieLang === 'undefined'){
        for(var i = 0; i < req.siteData.allLanguages.length; i++){
            if(req.siteData.allLanguages[i].id == '1'){
                res.cookie('lang', req.siteData.allLanguages[i].lang_code, {expire: 400000000000000 + Date.now()});
                req.siteData.lang = req.siteData.allLanguages[i].lang_code;
                req.siteData.translations = translations.translations[req.siteData.allLanguages[i].lang_code]
            }
        }
    }else{
        req.siteData.lang = cookieLang;
        req.siteData.translations = translations.translations[cookieLang]
    }

    next();
}

const getGames = function (req, res, next) {
    var cookieLang = req.cookies['lang'];
    var langId = statisMethods.detectLanguageId(cookieLang,req.siteData.allLanguages)
    const options = {
        url: `${statics.API_URL}/games/all/${langId}`,
        headers: {
            'authorization': statics.API_AUTH
        }
    };
    request(options, function (error, response, body) {
        const info = JSON.parse(body);
        if(!info.error){
            req.siteData.allGames = info.data;
        }
        next();
    });
}

// const setSocketConnection =  function (req, res, next, server) {
//     var io = require('socket.io')(server);
//     io.on('connection', (socket) => {
//         console.log('a user connected');
//
//         socket.emit('news', { hello: 'world' });
//
//         socket.on('my other event', function (data) {
//             console.log(data);
//         });
//
//         socket.on('disconnect', () => {
//             console.log('user disconnected');
//             socket.disconnect();
//         });
//         // next();
//     });
// }

// const setTranslations = function (req, res, next) {
//     var cookieLang = req.cookies['lang'];
//     var langId = statisMethods.detectLanguageId(cookieLang,req.siteData.allLanguages)
//     const options = {
//         url: `${statics.API_URL}/games/all/${langId}`,
//         headers: {
//             'authorization': statics.API_AUTH
//         }
//     };
//     request(options, function (error, response, body) {
//         const info = JSON.parse(body);
//         if(!info.error){
//             req.siteData.allGames = info.data;
//         }
//         next();
//     });
// }







module.exports.checkUser = checkUser;
module.exports.setlanguage = setlanguage;
module.exports.getGames = getGames;
module.exports.getAllLanguages = getAllLanguages;
// module.exports.setSocketConnection = setSocketConnection;
// module.exports.setTranslations = setTranslations;
