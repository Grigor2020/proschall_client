var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');

var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');
var authRouter = require('./routes/auth/auth');
var gamesRouter = require('./routes/games/games');
var termsRouter = require('./routes/terms/terms');
var challengeRouter = require('./routes/challenge/challenge');
var matchRouter = require('./routes/match/match');
var notificationRouter = require('./routes/notification/notification');
var profileRouter = require('./routes/profile/profile');
var forgotRouter = require('./routes/forgot/forgot');
var tournamentRouter = require('./routes/tournament/tournament');

var mids = require('./middleware')

const request = require('request');
var statics = require('./static');

const mongoose = require('mongoose')
mongoose.connect('mongodb://127.0.0.1:27017/prizchall-scheduler', { useNewUrlParser: true, useUnifiedTopology: true });

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));


app.enable('trust proxy')
app.use(function (req, res, next) {
    req.siteData = {};
    // var ip = req.connection.remoteAddress;
    // console.log('ip',ip)
    res.setHeader("Access-Control-Allow-Headers", "x-access-token,void,authorization");
    res.setHeader("Access-Control-Allow-Origin", "*");
    res.setHeader("Access-Control-Allow-Credentials", true);
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT'); // OPTIONS, PATCH,
    res.setHeader('Cache-Control', 'no-cache'); // OPTIONS, PATCH,
    res.setHeader('X-Frame-Options', 'deny'); // OPTIONS, PATCH,
    res.removeHeader('Server');
    res.removeHeader('X-Powered-By');
    next()
})

app.locals.MyNameLAstname = 'Grigor Sadoyan';
app.use(function (req, res, next) {mids.getAllLanguages(req, res, next)});
app.use(function (req, res, next) {mids.setlanguage(req, res, next)});
app.use(function (req, res, next) {mids.getGames(req, res, next)});
app.use(function (req, res, next) {mids.checkUser(req, res, next)});


app.use('/', indexRouter);
app.use('/users', usersRouter);
app.use('/auth', authRouter);
app.use('/terms', termsRouter);
app.use('/games', gamesRouter);
app.use('/challenge', challengeRouter);
app.use('/match', matchRouter);
app.use('/notification', notificationRouter);
app.use('/profile', profileRouter);
app.use('/forgot', forgotRouter);
app.use('/tournament', tournamentRouter);


// catch 404 and forward to error handler
app.use(function(req, res, next) {
    next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
    // set locals, only providing error in development
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'development' ? err : {};

    // render the error page
    res.status(err.status || 500);
    res.render('error');
});

module.exports = app;