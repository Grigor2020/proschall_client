// $('.lang_area').click(function (){

// })
$(document).ready(function () {
    $( "#disputeFile" ).change(function() {
        console.log('img_name - ',this.files[0].name)
    });


    $("header .menu-btn").click(function () {
        var $this = $(this);
        if (!$this.hasClass("active")) {
            $this.addClass("active");
            $(".left-navigation").addClass("open");
            $("header").addClass("active");
            $(".logo-area .small-logo").addClass("d-none");
            $(".logo-area .big-logo").removeClass("d-none");
            setTimeout(function () {
                $(".left-navigation .games-list .item .name").fadeIn(100);
                $(".left-navigation .navigation ul").removeClass("d-none");
                $(".left-navigation  .bottom-info .info-inner").removeClass("d-none");
            }, 200);
            $("header .layer").removeClass("d-none");
        }
        else {
            $this.removeClass("active");
            $(".left-navigation").removeClass("open");
            $("header").removeClass("active");
            $(".logo-area .small-logo").removeClass("d-none");
            $(".logo-area .big-logo").addClass("d-none");
            $(".left-navigation .games-list .item .name").fadeOut(0);
            $(".left-navigation .navigation ul").addClass("d-none");
            $(".left-navigation .bottom-info .info-inner").addClass("d-none");
            $("header .layer").addClass("d-none");
        }
    });

    $(".resetPass").click(function(){
        $(".popup-outer .tab-item.active").removeClass("active");
        $(".popup-outer .tab-link.active").removeClass("active");
        $(".popup-outer").fadeOut(300);
        $(".popup-outer").removeClass("active");
        setTimeout(function(){
            $("#resetPasswordPopup").addClass("active").fadeIn(300);
        }, 300);
    });

    // $(document).on('click',)
    $(document).on("click", "header .buttons-area .user-info-area .username-btn", function() {
        var $this = $(this);
        var $popup = $this.closest(".user-info-area").find(".user-info");
        var $triangle = $this.closest(".user-info-area").find(".triangle");
        if (!$this.hasClass("active")) {
            if ($("header .buttons-area .notification-btn.active").length > 0) {
                $("header .buttons-area .triangle").fadeOut(300);
                $("header .buttons-area .notifications.active").removeClass("active").fadeOut(300);
                $("header .buttons-area .notification-btn.active").removeClass("active");
                $("header .buttons-area .notification-area .n-list .item.active .notification.active").fadeOut(300).removeClass("active");
                $("header .buttons-area .notification-area .n-list .item.active").removeClass("active");
            }
            $this.addClass("active");
            $popup.addClass("active").fadeIn(300);
            $triangle.fadeIn(300);
        }
        else {
            $triangle.fadeOut(300);
            $popup.removeClass("active").fadeOut(300);
            $this.removeClass("active");
        }
    });
    $(document).on('click',"header .buttons-area .notification-btn", function() {
        // console.log('erererre');
        var $this = $(this);
        var $notifications = $this.closest(".notification-area").find(".notifications");
        var $triangle = $this.closest(".notification-area").find(".triangle");
        if($("header .buttons-area .notification-area .n-list .item").length != 0){
            if (!$this.hasClass("active")) {
                if ($("header .buttons-area .user-info-area .username-btn.active").length > 0) {
                    $("header .buttons-area .user-info-area .triangle").fadeOut(300);
                    $("header .buttons-area .user-info-area .user-info.active").removeClass("active").fadeOut(300);
                    $("header .buttons-area .user-info-area .username-btn").removeClass("active");
                }
                $this.addClass("active");
                $notifications.addClass("active").fadeIn(300);
                $triangle.fadeIn(300);
            }
            else {
                $triangle.fadeOut(300);
                $notifications.removeClass("active").fadeOut(300);
                $this.removeClass("active");
                $("header .buttons-area .notification-area .n-list .item.active .notification.active").fadeOut(300).removeClass("active");
                $("header .buttons-area .notification-area .n-list .item.active").removeClass("active");
            }
        }
    });

    if(parseInt($("header .buttons-area .notification-area .notification-count").text()) != 0){
        $("header .buttons-area .notification-area .notification-count.d-none").removeClass("d-none");
    }
    $(document).on('click',"header .buttons-area .notification-area .n-list .item", function(e) {
        var $this = $(this);
        var $notification = $this.find(".notification");
        var $notificationCount = parseInt($("header .buttons-area .notification-area .notification-count").text());
        var $notifyStatus = $this.find(".status");
        var $notifyId = $this.attr('data-notify-id');
        if (!$this.hasClass("active")) {
            if($notifyStatus.hasClass("unread")){
                let url = "/notification/set-view"
                let body = "notifyId="+$notifyId;
                requestPost(url,body,function(){
                    if (this.readyState == 4) {
                        let result = JSON.parse(this.responseText);
                        if(!result.error){
                            $notificationCount--;
                                $("header .buttons-area .notification-area .notification-count").text($notificationCount)
                                $notifyStatus.removeClass("unread");
                                if($notificationCount != 0){
                                    $("header .buttons-area .notification-area .notification-count.d-none").removeClass("d-none");
                                }
                                else{
                                    $("header .buttons-area .notification-area .notification-count").addClass("d-none");
                                }
                        }
                        hideLoad();
                    }
                })

            }
            $("header .buttons-area .notification-area .n-list .item.active .notification.active").fadeOut(300).removeClass("active");
            $("header .buttons-area .notification-area .n-list .item.active").removeClass("active");
            $this.addClass("active");
            $notification.fadeIn(300).addClass("active");
        }
        else {
            if ($(e.target).closest(".notification.active").length === 0) {
                $notification.fadeOut(300).removeClass("active");
                $this.removeClass("active");
            }
        }
    });

    // $(".filter-arae").on("click", "ul a[href='javascript:;']", function(){
    //     let $this = $(this);
    //     $this.closest("ul").find("a.active").removeClass("active");
    //     $this.addClass("active");
    //     $this.closest(".filter-item").find(".label a").text($this.text());
    //     $this.closest(".filter-item").find(".label.active").removeClass("active");
    //     $this.closest("ul").slideUp(300).removeClass("active");
    //     if($this.attr("data-gid") !== undefined){
    //         $(".cards-list .card.active").fadeOut(0).removeClass("active");
    //         $(".cards-list:not(.by-game)").addClass("by-game");
    //         if($(".cards-list.by-console").length == 0){
    //             $(`.cards-list .card[data-gid="${$this.attr("data-gid")}"]`).fadeIn(300).addClass("active");
    //         }
    //         else{
    //             $(`.cards-list .card[data-gid="${$this.attr("data-gid")}"][data-cid="${$("#filterConsoles a.active").attr("data-cid")}"]`).fadeIn(300).addClass("active");
    //         }
    //     }
    //     else if($this.attr("data-cid") !== undefined){
    //         $(".cards-list .card.active").fadeOut(0).removeClass("active");
    //         $(".cards-list:not(.by-console)").addClass("by-console");
    //         if($(".cards-list.by-game").length == 0){
    //             $(`.cards-list .card[data-cid="${$this.attr("data-cid")}"]`).fadeIn(300).addClass("active");
    //         }
    //         else{
    //             $(`.cards-list .card[data-cid="${$this.attr("data-cid")}"][data-gid="${$("#filterGames a.active").attr("data-gid")}"]`).fadeIn(300).addClass("active");
    //         }
    //     }
    //     else if($this.attr("data-prize") !== undefined){
    //         let arr = [];
    //         $(".cards-list .card.active").each(function(){
    //             let $this = $(this);
    //             let gid = $this.attr("data-id");
    //             let prize = parseFloat($this.attr("data-prize"));
    //             arr.push({gid : gid,prize: prize});
    //         });
    //         if($this.attr("data-prize") == "min-max"){
    //             arr.sort((a,b) => a.prize-b.prize);
    //         }
    //         else if($this.attr("data-prize") == "max-min"){
    //             arr.sort((a,b) => b.prize-a.prize);
    //         }
    //         $.each(arr, function(key, value){
    //             $(".card[data-id='" + value.gid + "']").css("order", key);
    //         })
    //     }
    // });

    $(".questions-section .questions-list .item .tab-header a").click(function () {
        var $item = $(this).closest(".item");
        var $text = $item.find(".tab-content");
        if (!$item.hasClass("active")) {
            $(".questions-section .questions-list .item.active .tab-content").slideUp(300);
            $(".questions-section .questions-list .item.active").removeClass("active");
            $item.addClass("active");
            $text.slideDown(300);
        }
        else {
            $text.slideUp(300);
            $item.removeClass("active");
        }
    });

    $(".popup-outer .tab-header .tab-link").click(function () {
        var $this = $(this);
        if (!$this.hasClass("active")) {
            $(".popup-outer .tab-header .tab-link.active").removeClass("active");
            $(".popup-outer .tab-item.active").removeClass("active");
            $this.addClass("active");
            $(".popup-outer .tab-item[data-tab='" + $this.attr("data-tab") + "']").addClass("active");
        }
    });

    $("header .buttons-area.logout a, .sign-btn").click(function () {
        var $this = $(this);
        $(".popup-outer .tab-item[data-tab='" + $this.attr("data-tab") + "']").addClass("active");
        $(".popup-outer .tab-link[data-tab='" + $this.attr("data-tab") + "']").addClass("active");
        $(".popup-outer").fadeIn(300);
        $(".popup-outer").addClass("active")
    });

    $(".popup-outer .btn-close").click(function () {
        $(".popup-outer .tab-item.active").removeClass("active");
        $(".popup-outer .tab-link.active").removeClass("active");
        $(".popup-outer").fadeOut(300);
        $(".popup-outer").removeClass("active");
    });

    // $(".filter-arae .filter-item .label").click(function () {
    //     var $this = $(this);
    //     var $list = $this.closest(".filter-item").find("ul");
    //     if (!$this.hasClass("active")) {
    //         $(".filter-arae .filter-item ul.active").slideUp(300);
    //         $(".filter-arae .filter-item ul.active").removeClass("active");
    //         $(".filter-arae .filter-item .label").removeClass("active");
    //         $this.addClass("active");
    //         $list.addClass("active");
    //         $list.slideDown(300);
    //     }
    //     else {
    //         $this.removeClass("active");
    //         $list.slideUp(300);
    //         $list.removeClass("active");
    //     }
    // });

    if ($(".input-field .current").length > 0) {
        $(".input-field .current").click(function () {
            var $this = $(this);
            var $list = $this.closest(".input-field").find(".list");
            if (!$this.hasClass("active")) {
                $(".input-field .current.active").closest(".input-field").find(".list").slideUp(300).removeClass("active");
                $(".input-field .current.active").removeClass("active");
                $this.addClass("active");
                $list.addClass("active");
                $list.slideDown(300);
            }
            else {
                $list.slideUp(300);
                $this.removeClass("active");
                $list.removeClass("active");
            }
        });

        $(".list").on("click", "div:not(.add-option)", function () {
            var $this = $(this);
            var $currentTxt = $this.closest(".input-field").find(".current.active");
            var $list = $this.closest(".input-field").find(".list.active");
            if (!$this.hasClass("selected")) {
                $(".list.active div.selected").removeClass("selected");
                $this.addClass("selected");
                $currentTxt.html($this.html()).removeClass("active");
                $this.closest(".input-fields").find(".input-field").eq(0).find(".current").addClass("checked")
                $list.slideUp(300).removeClass("active");
            }
        });
    }

    $("header .lng-area .current a").click(function () {
        var $this = $(this);
        var $otherLng = $this.closest(".lng-area").find(".other");
        if (!$this.hasClass("active")) {
            $this.addClass("active");
            $otherLng.slideDown(300).addClass("active");
        }
        else {
            $otherLng.slideUp(300).removeClass("active");
            $this.removeClass("active");
        }


    })
    $("header .lng-area .other a").click(function () {
        var $this = $(this);
        document.cookie = "lang="+$this.attr('data-lang')+"; expires=Thu, 18 Dec 2030 12:00:00 UTC; path=/";
        window.location.reload()
    });
    $(".match-detail-page .dispute-btn").click(function () {
        var $this = $(this);
        $this.closest("html").find("#disputePopup").addClass("active").fadeIn(300);
    });

    $(".tab-link a").click(function () {
        var $this = $(this);
        var $tabLink = $this.closest(".tab-link");
        if (!$tabLink.hasClass("active")) {
            $(".tab-link.active").removeClass("active");
            $(".tab-content .tab.active").removeClass("active");
            $tabLink.addClass("active");
            $(".tab-content .tab[data-tab='" + $this.attr("data-tab") + "']").addClass("active");
        }
    });

    if ($("main").hasClass("browse-match-page")) {
        $(".games-list .item").each(function () {
            var $this = $(this);
            if (window.location.pathname == $this.find("a").attr("href")) {
                $this.addClass("active")
            }
        })
    }

    if ($("main").hasClass("deposit-page")) {
        $(".tab-item a[href='" + window.location.hash + "']").addClass("active");
        $(".tab-section[data-section='" + window.location.hash.replace("#", "") + "']").addClass("active");

        $(".tab-item a").click(function () {
            var $this = $(this);
            if (!$this.hasClass("active")) {
                $(".tab-item a.active").removeClass("active");
                $this.addClass("active");
                $(".tab-section.active").removeClass("active");
                $(".tab-section[data-section='" + $this.attr("data-link") + "']").addClass("active");
            }
        });
    }

    if ($("main").hasClass("home-main")) {
        $(".left-navigation .navigation a[data-nav='faq']").click(function () {
            var $this = $(this);
            $("header .menu-btn").removeClass("active");
            $(".left-navigation").removeClass("open");
            $("header").removeClass("active");
            $(".logo-area .small-logo").removeClass("d-none");
            $(".logo-area .big-logo").addClass("d-none");
            $(".left-navigation .games-list .item .name").fadeOut(0);
            $(".left-navigation .navigation ul").addClass("d-none");
            $(".left-navigation .bottom-info .info-inner").addClass("d-none");
            $("header .layer").addClass("d-none");
            $("html, body").animate({ scrollTop: $("section.questions-section").offset().top - $("header").outerHeight() })
        });
    }
    else {
        $(".left-navigation .navigation a[data-nav='faq']").closest("li").remove();
    }

    if ($("main").hasClass("create-match-page")) {

        $(".input-field .add-option .btn-add").click(function (event) {
            event.preventDefault();
            var $this = $(this);
            var $input = $this.closest(".add-option").find(".add-input");
            if ($input.val().trim() !== "") {
                $this.closest(".list").prepend("<div>" + $input.val() + "</div>");
                $input.val("")
            }
        });


        $("#btnAddUsername").click(function () {
            var $this = $(this);
            var inputValue = $this.closest(".input-area").find("input").val();
            addGameUsername(inputValue,'', function (addGameUsernameData) {
                // console.log('addGameUsernameData', addGameUsernameData)
                addChallenge(function (res) {
                    // console.log('resss',res)
                    let $addUsernamePopup = $("#addUsernamePopup")
                    if (!res.error) {
                        if (res.msg === '0000' && res.msg !== "24444" && res.msg !== "24555") {
                            $("#createMatchSuccessNotify").addClass("active").fadeIn(300);
                            setTimeout(function () {
                                window.location.pathname = "/challenge/my"
                            }, 2000);
                        }
                        if (res.msg === "33659") {
                            $addUsernamePopup.addClass("active").fadeIn(300);
                        }
                        if(res.msg === "24444"){
                            $addUsernamePopup.removeClass("active").fadeIn(300);
                            $addUsernamePopup.addClass("d-none")
                            $(".min-prize-error:not(.d-none)").addClass("d-none");
                            $(".no-money-error.d-none").removeClass("d-none");
                        }
                        if(res.msg === "24555"){
                            $(".no-money-error:not(.d-none)").addClass("d-none");
                            $(".min-prize-error.d-none").removeClass("d-none");
                        }
                        if(res.msg === "28902"){
                            $("#createMatchErrorPopup").addClass("active").fadeIn(300);
                        }
                    }else{
                        $("#createMatchErrorPopup").addClass("active").fadeIn(300);
                    }
                    hideLoad()
                })
            })
        });

        $("#btnCreateMatch").click(function (event) {
            event.preventDefault();
            createChallange();
        });

        // $(document).on('keyup',function(e) {
        //     if(e.which == 13) {
        //         createChallange();
        //     }
        // });

        $('.datepicker-here').datepicker({
            language: 'en'
        });
    }

    if ($("main").hasClass("profile-page")) {
        $(".games-list .input-area button").click(function () {
            var $this = $(this);
            var $input = $this.closest(".input-area").find("input");
            var $inputVal = $this.closest(".input-area").find("input").val().trim();
            var $p = $this.closest(".input-area").find("p");
            var $gameId = $this.closest(".item").attr("data-game");
            if ($inputVal != "") {
                var txt = $input.val().trim();
                addGameUsername($inputVal, $gameId, function (result) {});
                $this.addClass("d-none");
                $input.addClass("d-none");
                $p.removeClass("d-none");
                $p.text(txt);
            }
        });
    }

    $(document).on("click", ".notify-btn", function () {
        var $this = $(this);
        let noteType = $this.attr('data-notify-type');
        if(noteType == '4'){
            let matchId = $this.attr('data-match-id');
            let url = '/match/set-accept';
            let body = 'matchId='+matchId+'&accept=1';
            requestPost(url, body, function () {
                if (this.readyState == 4) {
                    let result = JSON.parse(this.responseText);
                    if (!result.error) {
                        hideLoad();
                        window.location.href = `/match/${matchId}`
                    } else {
                        if(result.msg === "58249") {
                            alert("You have joined to another match")
                        }
                    }
                }
            })
        }
        if(noteType == '0' || noteType == '7'){
            let matchId = $(this).attr('data-match-id');
            let url = '/match/set-start';
            let body = 'matchId='+matchId;
            requestPost(url, body, function () {
                if (this.readyState == 4) {
                    let result = JSON.parse(this.responseText);
                    if (!result.error) {
                        hideLoad();
                        window.location.href = `/match/${matchId}`
                    }
                }
            })
        }
    })

    $(".match-detail-page .match-detail").on("click", ".start-game-btn", function () {
        let $this = $(this);
        let matchId = $this.attr('data-match-id');
        let url = '/match/set-start';
        let body = 'matchId='+matchId;
        requestPost(url, body, function () {
            if (this.readyState == 4) {
                let result = JSON.parse(this.responseText);
                if (!result.error) {
                    hideLoad();
                    window.location.href = `/match/${matchId}`
                }
            }
        })
    });

    $("#c2cForm").on('click', '#btnC2C', function(e){
        e.preventDefault();
        $("#c2cForm .validation-error-msg:not(.d-none)").addClass("d-none");
        if(($("#c2cForm #cardNumber").val().trim() == "" || $("#c2cForm #cardNumber").val().length != 16) && ($("#c2cForm #amount").val().trim() == "" || $("#c2cForm #amount").val() <= 0)){
            $("#c2cForm .validation-error-msg.d-none").removeClass("d-none");
        }
        else if($("#c2cForm #cardNumber").val().trim() == "" || $("#c2cForm #cardNumber").val().length != 16){
            $("#c2cForm .validation-error-msg.err-c2c-card-number.d-none").removeClass("d-none");
        }
        else if($("#c2cForm #amount").val().trim() == "" || $("#c2cForm #amount").val() <= 0){
            $("#c2cForm .validation-error-msg.err-c2c-amount.d-none").removeClass("d-none");
        }
        else{
            $("#c2cForm").submit();
        }
    });

    $("#bitcoinForm").on('click', '#btnBitcoin', function(e){
        e.preventDefault();
        $("#bitcoinForm .validation-error-msg:not(.d-none)").addClass("d-none");
        if($("#bitcoinForm #walletAddress").val().trim() == "" && ($("#bitcoinForm #bitAmount").val().trim() == "" || $("#bitcoinForm #bitAmount").val() <= 0)){
            $("#bitcoinForm .validation-error-msg.d-none").removeClass("d-none");
        }
        else if($("#bitcoinForm #bitAmount").val().trim() == "" || $("#bitcoinForm #bitAmount").val() <= 0){
            $("#bitcoinForm .validation-error-msg.err-bit-amount.d-none").removeClass("d-none");
        }
        else if($("#bitcoinForm #walletAddress").val().trim() == ""){
            $("#bitcoinForm .validation-error-msg.err-wallet-address.d-none").removeClass("d-none");
        }
        else{
            $("#bitcoinForm").submit();
        }
    });

    $("#pmForm").on('click', '#btnperfectMoney', function(e){
        e.preventDefault();
        $("#pmForm .validation-error-msg:not(.d-none)").addClass("d-none");
        if($("#pmForm #eVoucher").val().trim() == "" && $("#pmForm #activationCode").val().trim() == ""){
            $("#pmForm .validation-error-msg.d-none").removeClass("d-none");
        }
        else if($("#pmForm #eVoucher").val().trim() == ""){
            $("#pmForm .validation-error-msg.err-voucher.d-none").removeClass("d-none");
        }
        else if($("#pmForm #activationCode").val().trim() == ""){
            $("#pmForm .validation-error-msg.err-voucher-activation-code.d-none").removeClass("d-none");
        }
        else{
            $("#pmForm").submit();
        }
    });

    $(".match-detail-page .match-detail").on("click", "#acceptBtn", function () {
        let $this = $(this);
        let matchId = $this.attr('data-match-id');
        let url = '/match/set-accept';
        let body = 'matchId='+matchId+'&accept=1';
        requestPost(url, body, function () {
            if (this.readyState == 4) {
                let result = JSON.parse(this.responseText);
                if (!result.error) {
                    hideLoad();
                    window.location.href = `/match/${matchId}`
                } else {
                    if(result.msg === "58249") {
                        alert("You have joined to another match")
                    }
                }
            }
        })
    })

    $("#btnDisputeSubmit").click(function(e){
        e.preventDefault();
        var gameResult = $("#disputePopup form .input-field .current").text().trim() != "" ? $("#disputeGameResultHidden").val($("#disputePopup form .input-field .current").text()) : "";
        var explanation = $('#disputePopup form .input-field .message textarea').val();
        $(".validation-error-msg:not(.d-none)").addClass("d-none");
        if(explanation.trim() != "" && gameResult != ""){
            $("#mId").val($(".match-detail-page .match-detail .card #matchId span").text());
            $("#disputeForm").submit();
        }
        else if(gameResult == ""){
            $(".validation-error-msg.err-result.d-none").removeClass("d-none");
        }
        else if(explanation.trim() == ""){
            $(".validation-error-msg.err-explanation.d-none").removeClass("d-none");
        }
    //      var fd = new FormData();
    //      var files = $('#disputeFile')[0].files[0];
    //      console.log('files',files)
    //      fd.append('file',files);
    //      console.log('fd 2',fd)
    //     $.ajax({
    //         url: '/profile/dispute',
    //         type: 'post',
    //         data: {a:'a',b:'b'},
    //         contentType: false,
    //         processData: false,
    //     success: function(response){
    //         if(response != 0){
    //             $("#img").attr("src",response);
    //             $(".preview img").show(); // Display image element
    //         }else{
    //             alert('file not uploaded');
    //         }
    //      },
    //    });
    // requestPost('/profile/dispute','file=')
      });

    $('.cancel-btn').click(function () {
        let matchId = $(this).attr('data-match-id');
        let url = '/match/set-decline';
        let body = 'matchId='+matchId;
        requestPost(url, body, function () {
            if (this.readyState == 4) {
                let result = JSON.parse(this.responseText);
                if (!result.error) {
                    hideLoad();
                    window.location.href = `/`
                }
            }
        })
    })

    $(".personal-detail-section #personalDetailForm #btnVerifyEmail").click(function (event) {
        event.preventDefault();
        $("#verifyEmailSuccessNotify").addClass("active").fadeIn(300);
        setTimeout(function () {
            $("#verifyEmailSuccessNotify").fadeOut(300).removeClass("active");
        }, 2000)
    });

    $("footer .footer-navigation .item[data-href='faq']").click(function () {
        if ($("section.questions-section").length > 0 && $("section.questions-section.d-none").length == 0) {
            $("html, body").animate({ scrollTop: $("section.questions-section").offset().top - $("header").outerHeight() })
        }
    });

    if ($("main").hasClass("browse-match-page")) {
        if ($("header .buttons-area.login.d-none").length === 0) {
            $(".questions-section").addClass("d-none");
            $(".about-section").addClass("d-none");
            $(".gamer-dashboard-bg.d-none").removeClass("d-none");
            $(".browse-match-section").addClass("is-logged");
        }
        else {
            $(".cards-list-area>.button-area.d-none").removeClass("d-none");
            $(".section-title.d-none").removeClass("d-none");
            $(".browse-match-section .header-img.d-none").removeClass("d-none");
            $(".questions-section.d-none").removeClass("d-none");
            $(".about-section.d-none").removeClass("d-none");
            $(".browse-match-section.is-logged").removeClass("is-logged");
        }
    }

    $(document).on("click", function (e) {
        if ($(".left-navigation").hasClass("open") && $(e.target).closest(".left-navigation.open").length === 0 && $(e.target).closest(".menu-btn").length === 0) {
            $("header .menu-btn").removeClass("active");
            $(".left-navigation").removeClass("open");
            $("header").removeClass("active");
            $(".logo-area .small-logo").removeClass("d-none");
            $(".logo-area .big-logo").addClass("d-none");
            $(".left-navigation .games-list .item .name").fadeOut(0);
            $(".left-navigation .navigation ul").addClass("d-none");
            $(".left-navigation  .bottom-info .info-inner").addClass("d-none");
            $("header .layer").addClass("d-none");
        }
        if ($(".popup-outer").hasClass("active") && $(e.target).closest(".popup-content").length === 0 && $(e.target).closest("header .buttons-area").length === 0 && $(e.target).closest("#match-join").length === 0 && $(e.target).closest(".sign-btn").length === 0) {
            $(".popup-outer .tab-item.active").removeClass("active");
            $(".popup-outer .tab-link.active").removeClass("active");
            $(".popup-outer").fadeOut(300);
            $(".popup-outer").removeClass("active");
        }
        // if ($(".filter-arae .filter-item .label").hasClass("active") && $(e.target).closest(".filter-arae .filter-item").length === 0 && $(e.target).closest(".filter-arae .filter-item ul").length === 0) {
        //     $(".filter-arae .filter-item ul.active").slideUp(300);
        //     $(".filter-arae .filter-item ul.active").removeClass("active");
        //     $(".filter-arae .filter-item .label").removeClass("active");
        // }
        if($("#createMatchErrorPopup").hasClass('active') && $(e.target).closest("#createMatchErrorPopup").length === 0){
            $("#createMatchErrorPopup").fadeOut(300).removeClass("active");
        }
        if ($("header .buttons-area .user-info-area .user-info").hasClass("active") && $(e.target).closest(".user-info-area .user-info").length === 0 && $(e.target).closest(".user-info-area .username-btn").length === 0) {
            $("header .buttons-area .user-info-area .triangle").fadeOut(300);
            $("header .buttons-area .user-info-area .user-info.active").removeClass("active").fadeOut(300);
            $("header .buttons-area .user-info-area .username-btn").removeClass("active");
        }
        if ($("header .buttons-area .notifications").hasClass("active") && $(e.target).closest(".notification-area .notifications").length === 0 && $(e.target).closest(".notification-area .notification-btn").length === 0 && $(e.target).closest(".notification-area .notification").length === 0) {
            $("header .buttons-area .triangle").fadeOut(300);
            $("header .buttons-area .notifications.active").removeClass("active").fadeOut(300);
            $("header .buttons-area .notification-btn.active").removeClass("active");
            $("header .buttons-area .notification-area .n-list .item.active .notification.active").fadeOut(300).removeClass("active");
            $("header .buttons-area .notification-area .n-list .item.active").removeClass("active");
        }
        if ($(e.target).closest("form .input-field .current.active").length === 0 && $(e.target).closest("form .input-field .list.active").length === 0) {
            $("form .input-field .current.active").removeClass("active");
            $("form .input-field .list.active").slideUp(300).removeClass("active");
        }
        if ($(e.target).closest(".popup-dispute").length === 0 && $(e.target).closest("#btnDispute").length === 0 && $(e.target).closest(".dispute-btn").length === 0) {
            $("#disputePopup").fadeOut(300).removeClass("active");
        }
        if ($(e.target).closest(".lng-area").length === 0 && $(e.target).closest(".lng-area .current").length === 0) {
            $(".lng-area .other.active").slideUp(300).removeClass("active");
            $(".lng-area .current a.active").removeClass("active");
        }
    });

    $("#btnSignUp").click(function () {
        var $fname_error_field = $('.fname_error_field');
        var $lname_error_field = $('.lname_error_field');
        var $username_error_field = $('.username_error_field');
        var $email_error_field = $('.email_error_field');
        var $phone_error_field = $('.phone_error_field');
        var $password_error_field = $('.password_error_field');
        var $confirmPassword_error_field = $('.confirmPassword_error_field');
        var $termsConditions_error_field = $('.termsConditions_error_field');

        $fname_error_field.text('');
        $lname_error_field.text('');
        $username_error_field.text('');
        $email_error_field.text('');
        $phone_error_field.text('');
        $password_error_field.text('');
        $confirmPassword_error_field.text('');
        $termsConditions_error_field.text('');

        var firstName = $('#firstName').val();
        var lastName = $('#lastName').val();
        var username = $('#username').val();
        var email = $('#email').val();
        var phone = $('#phone').val();
        var password = $('#password').val();
        var confirmPassword = $('#confirmPassword').val();

        if (firstName === '') { $fname_error_field.text('firstName'); return false }
        if (lastName === '') { $lname_error_field.text('lastName'); return false }
        if (username === '') { $username_error_field.text('username'); return false }
        if (phone === '') { $phone_error_field.text('phone'); return false }
        if (!ValidateEmail(email)) { $email_error_field.text('email'); return false }
        if (password === '') { $password_error_field.text('password'); return false }
        if (password.length < 8) { $password_error_field.text('type more than 8'); return false }
        if (confirmPassword === '') { $confirmPassword_error_field.text('confirmPassword'); return false }
        if (confirmPassword.length < 8) { $confirmPassword_error_field.text('type more than 8'); return false }
        if (!checkPasswordsValid(password, confirmPassword)) {
            $password_error_field.text('passwords not same'); return false
        }
        if (!checkCheckBox('termsConditions')) {
            $termsConditions_error_field.text('check the checkbox '); return false
        }

        requestPost('/auth/check-data','username='+username+'&email='+email, function () {
            if (this.readyState == 4) {
                let $email_error_field = $('.email_error_field');
                let $username_error_field = $('.username_error_field');
                $email_error_field.text('')
                $username_error_field.text('')
                let result = JSON.parse(this.responseText);
                // console.log('check-data result',result)
                if (!result.error) {
                    let body = '' +
                        'firstName='+firstName+'&' +
                        'lastName='+lastName+'&' +
                        'username='+username+'&' +
                        'email='+email+'&' +
                        'phone='+phone+'&' +
                        'password='+password+'&' +
                        'confirmPassword='+confirmPassword+'';
                    requestPost('/auth/sign-up',body,function (){
                        if (this.readyState == 4) {
                            let regResult = JSON.parse(this.responseText);
                            // console.log('sign-up result',regResult)
                            if(!regResult.error){
                                hideLoad();
                                window.location.reload()
                            }
                        }
                    })
                }else{
                    if(result.msg.email === true){
                        $email_error_field.text('email exists');
                    }
                    if(result.msg.username === true ){
                        $username_error_field.text('username exists');
                    }
                    hideLoad();
                    return  false
                }
            }
        })
    });

    $("#btnSignIn").click(function () {
        let $loginErrorField = $('#login-error');
        let $password_error_field_l = $('.password_error_field_l');
        let $email_error_field_l = $('.email_error_field_l');

        $loginErrorField.text('');
        $email_error_field_l.text('');
        $password_error_field_l.text('');

        var email = $('#signinEmail').val();
        var password = $('#signinPassword').val();

        if (password === '') { $password_error_field_l.text('password'); return false };
        if (password.length < 8) { $password_error_field_l.text('type more than 8'); return false };

        if (password === '') { $password_error_field_l.text('password'); return false };
        let body = 'login='+email+'&password='+password;
        requestPost('/auth/sign-in',body,function (){
            if (this.readyState == 4) {
                let regResult = JSON.parse(this.responseText);
                if(!regResult.error){
                    window.location.reload()
                }else{
                    if(regResult.errorCode == 'no user'){
                        $loginErrorField.text('Login or password are Incorrect')
                    }
                }
                hideLoad();
            }
        })


        $("#signinForm").submit();
    });

    const checkUpdateingPassord = (data,fields)=>{
        let error = false;
        if(data.oldPassword.length < 8){
            $(fields.profileOldPasswordError).text('old password is too short')
            error = true
        }else{
            $(fields.profileOldPasswordError).text('')
        }

        if(data.newPassword.length < 8){
            $(fields.profileNewPasswordError).text('new password is too short')
            error = true
        }else {
            $(fields.profileNewPasswordError).text('')
        }

        if(data.confirmPassword.length < 8){
            $(fields.profileConfirmPasswordError).text('confirm password is too short')
            error = true
        }else {
            $(fields.profileConfirmPasswordError).text('')
        }

        if(data.newPassword !== data.confirmPassword){
            $(fields.profileConfirmPasswordError).text('passwords are not same')
            error = true
        }else {
            $(fields.profileConfirmPasswordError).text('')
        }
        return error
    }

    $(document).on('click','#accountSecurityForm #btnChangePwd', async function (e) {
        e.preventDefault();
        const oldPassword = $('#oldPassword').val();
        const newPassword = $('#newPassword').val();
        const confirmPassword = $('#confirmPassword').val();
        const data = { oldPassword,newPassword,confirmPassword };

        const profileOldPasswordError = $('#profileOldPasswordError')
        const profileNewPasswordError = $('#profileNewPasswordError')
        const profileConfirmPasswordError = $('#profileConfirmPasswordError')
        const fields = { profileOldPasswordError,profileNewPasswordError,profileConfirmPasswordError }
        // let errorRes = await checkUpdateingPassord(data,fields)
        let ress = await checkUpdateingPassord(data,fields)
        if(!ress){
            requestPost('/profile/change-password','oldPassword='+oldPassword+'&newPassword='+newPassword+'', function (){
                if (this.readyState == 4) {
                    let regResult = JSON.parse(this.responseText);
                    if(!regResult.error){
                        alert('ok')
                        window.location.href = '/auth/logout';
                    }else{
                        alert('no')
                        hideLoad();
                    }

                }
            })
        }
    })

    $('#match-join').click(function () {
        let $this = $(this);
        if($this.closest("html").find("#loginPopUp").length > 0){
            $(".popup-outer .tab-item[data-tab='signin']").addClass("active");
            $(".popup-outer .tab-link[data-tab='signin']").addClass("active");
            $(".popup-outer").fadeIn(300).addClass("active");
        }
        else{
            var challengeId = $(this).attr('data-challenge-id');
            joinMatch(challengeId,function (result) {
                // console.log('result',result)
                setInterval(function(){
                    window.location.reload()
                }, 2000)

            })

        }
    });


    function joinMatch(challengeId,cb) {
        let body = "challengeId=" + challengeId;
        let url = "/match/join";
        requestPost(url, body, function () {
            if (this.readyState == 4) {
                let result = JSON.parse(this.responseText);
                hideLoad();
                if (!result.error) {
                    if (result.msg === "0000") {
                        $("#joinSuccessPopup").addClass("active").fadeIn(300);
                        setTimeout(function(){
                            $("#joinSuccessPopup").fadeOut(300).removeClass("active");
                        }, 2000)
                    }
                } else {
                    if (result.msg === "58108") {
                        $("#joinerPlayingErrorPopup").addClass("active").fadeIn(300);
                        setTimeout(function(){
                            $("#joinerPlayingErrorPopup").fadeOut(300).removeClass("active");
                        }, 2000)
                    }
                    if (result.msg === "58103") {
                        $("#gameCreateErrorPopup").addClass("active").fadeIn(300);
                        setTimeout(function(){
                            $("#gameCreateErrorPopup").fadeOut(300).removeClass("active");
                        }, 2000)
                    }
                    if (result.msg === "58183") {
                        $("#noMoneyErrorPopup").addClass("active").fadeIn(300);
                        setTimeout(function(){
                            $("#noMoneyErrorPopup").fadeOut(300).removeClass("active");
                        }, 2000)
                    }
                    if (result.msg === "58129") {
                        $("#noUsernamePopup").addClass("active").fadeIn(300);
                        setTimeout(function(){
                            window.location.href='/profile'
                        }, 10000)

                    }
                }
                hideLoad();
                cb(result)
            }
        })
    }


    $('.match-detail-page .game-btn').click(function () {
        var gameSituation = $(this).attr('data-status');
        var matchId = $(this).attr('data-match-id');
        let body = "matchId=" + matchId+'&gameSituation='+gameSituation;
        let url = "/match/situation";
        requestPost(url, body, function () {
            if (this.readyState == 4) {
                let result = JSON.parse(this.responseText);
                if(!result.error){
                    hideLoad();
                    window.location.reload()
                }
                else{
                    window.location.reload()
                }
            }
        })
    });

    $('.game-content').click(function () {
        $(this).addClass('active');
        checkcloseButton();
    })
    $('.close-game-content').click(function () {
        $('.game-content').removeClass('active');
        checkcloseButton();
    })

    let checkcloseButton = ()=> {
        let closeButton =  $('.close-game-content');
        if($('.game-content').hasClass('active')){
            closeButton.css({display:'flex'})
        }else{
            closeButton.css({display:'none'})
        }
    }

    // $('.game-content').click(function () {
    //     $(this).css({'width':'500px'})
    // })
    // $('.close-game-content').click(function () {
    //     $('.game-content').css({'width':'inherit'})
    // })

    // $('.cancel-btn').click(function () {
    //     var challengeId = $(this).attr('data-challenge-id');
    //     let body = "challengeId=" + challengeId;
    //     let url = "/match/join";
    //     requestPost(url, body, function () {
    //         if (this.readyState == 4) {
    //             let result = JSON.parse(this.responseText);
    //             if (!result.error) {
    //                 if (result.msg === "0000") {
    //                     $("#joinSuccessPopup").addClass("active").fadeIn(300);
    //                     setTimeout(function(){
    //                         $("#joinSuccessPopup").fadeOut(300).removeClass("active");
    //                     }, 2000)
    //                 }
    //             } else {
    //                 if (result.msg === "58108") {
    //                     $("#joinerPlayingErrorPopup").addClass("active").fadeIn(300);
    //                     setTimeout(function(){
    //                         $("#joinerPlayingErrorPopup").fadeOut(300).removeClass("active");
    //                     }, 2000)
    //                 }
    //                 if (result.msg === "58103") {
    //                     $("#gameCreateErrorPopup").addClass("active").fadeIn(300);
    //                     setTimeout(function(){
    //                         $("#gameCreateErrorPopup").fadeOut(300).removeClass("active");
    //                     }, 2000)
    //                 }
    //                 if (result.msg === "58183") {
    //                     $("#noMoneyErrorPopup").addClass("active").fadeIn(300);
    //                     setTimeout(function(){
    //                         $("#noMoneyErrorPopup").fadeOut(300).removeClass("active");
    //                     }, 2000)
    //                 }
    //             }
    //         }
    //     })
    // })
    resetPassword();
});

function addGameUsername(inputValue,game_id, cb) {
    let userName = inputValue;
    let gameId = game_id == '' ? $('#selectGame > div.selected').attr('data-game-id') : game_id ;
    let body = "game_id=" + gameId + "&userName=" + userName;
    let url = "/challenge/add-game-username";
    // console.log('body',body)
    requestPost(url, body, function () {
        if (this.readyState == 4) {
            let result = JSON.parse(this.responseText);
            hideLoad()
            cb(result);

        }
    })
}
function addChallenge(cb) {
    let game_id = $('#selectGame > div.selected').attr('data-game-id');
    let mode_id = $('#gameMode > div.selected').attr('data-mode-id');
    let prize = $('#prize-field').val();
    let server_id = $('#server > div.selected').attr('data-server-id');
    let note = $('#note').val();
    note = note.replace(/<br\s*\/?>/mg,"\n");
    if (typeof server_id === "undefined") {
        server_id = null;
    }
    let console_id = $('#byConsole > div.selected').attr('data-console-id');
    let body = "game_id=" + game_id + "&mode_id=" + mode_id + "&prize=" + prize + "&server_id=" + server_id + "&console_id=" + console_id + "&note=" + note;
    let url = "/challenge/create";
    // console.log('body', body)
    requestPost(url, body, function () {
        if (this.readyState == 4) {
            let result = JSON.parse(this.responseText);
            // console.log('............result',result)
            hideLoad();
            cb(result);
        }
    })
}

function createChallange(){
    $("#createMatchForm .validation-error-msg:not(.d-none)").addClass("d-none");
    var selfErr = false;
    $("#createMatchForm .input-field:not(.d-none)").each(function () {
        var $this = $(this);
        var $currentValue = $this.find(".current").length > 0 ? $this.find(".current").text() : $this.find("#prize-field").length > 0 ? $this.find("#prize-field").val() : $this.find("textarea").length > 0 ? $this.find("textarea").val() : "";
        var $errMsg = $this.find(".validation-error-msg.d-none");
        if ($currentValue.trim() == "") {
            $(".min-prize-error:not(.d-none)").addClass("d-none");
            $(".no-money-error:not(.d-none)").addClass("d-none");
            $errMsg.removeClass("d-none");
            selfErr = true;
        }
    });

    if (!selfErr) {
        addChallenge(function (res) {
            if (!res.error) {
                if (res.msg === '0000' && res.msg !== "24444" && res.msg !== "24555") {
                    $("#createMatchSuccessNotify").addClass("active").fadeIn(300);
                    setTimeout(function () {
                        window.location.pathname = "/challenge/my"
                    }, 2000);
                }
                if (res.msg === "33659") {
                    $("#addUsernamePopup").addClass("active").fadeIn(300);
                }
                if(res.msg === "24444"){
                    $(".min-prize-error:not(.d-none)").addClass("d-none");
                    $(".no-money-error.d-none").removeClass("d-none");
                }
                if(res.msg === "24555"){
                    $(".no-money-error:not(.d-none)").addClass("d-none");
                    $(".min-prize-error.d-none").removeClass("d-none");
                }
                if(res.msg === "28902"){
                    $("#createMatchErrorPopup").addClass("active").fadeIn(300);
                    // $("#userBusyErrorPopup").addClass("active").fadeIn(300);
                }
                setTimeout(function () {
                    // console.log('out')
                    $("#createMatchErrorPopup").removeClass("active");
                    $("#createMatchErrorPopup").css({display : "none"})
                    // $("#addUsernamePopup").removeClass("active");
                    // $("#addUsernamePopup").css({display : "none"})
                }, 2000)
            }else{
                // if(res.msg === "28902"){
                //     $("#userBusyErrorPopup").addClass("active").fadeIn(300);
                // }
            }
        });
    }
}

function ValidateEmail(mail) {
    if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(mail)) {
        return true
    }
    return false
}
function checkPasswordsValid(pass, rePass) {
    if (pass === rePass) {
        return true
    }
    return false
}
function checkCheckBox(id) {
    if (document.getElementById(id).checked == true) {
        return true
    }
    return false
}
function resetPassword(){
    const loader = `
    <div class="loader-area">
        <div class="loader">
            <svg viewBox="0 0 100 100">
                <circle class="spinner" style="fill:transparent;stroke:#FFB608;stroke-width: 7px;stroke-linecap: round;" cx="50" cy="50" r="45"/>
            </svg>
        </div>
    </div>`;
    const resetPassBtn = `<button id="resetPassBtn" class="btn-yellow">Send</button>`;
    const resetPassConfirmBtn = `<button id="resetPassConfirmBtn" class="btn-yellow">Confirm</button>`;
    const resetPassSaveBtn = `<button id="resetPassSaveBtn" class="btn-yellow">Confirm</button>`;

    $(".popup-container").on("click", "#resetPassBtn", function(e){
        e.preventDefault();
        const $this = $(this);
        let inputVal = $this.closest(".input-area").find("input").val();
        if(inputVal.trim() != "" && ValidateEmail(inputVal)){
            $this.closest(".input-area").find(".validation-error-msg:not(.d-none)").addClass("d-none");
            $this.closest(".input-area").find("form").append(loader);
            $this.remove();
            let email = inputVal;
            let url = '/forgot/send-email';
            let body = 'email=' + email;
            requestPost(url, body, function () {
                if (this.readyState == 4) {
                    hideLoad();
                    let result = JSON.parse(this.responseText);
                    $(".reset-pass-email").addClass("d-none");
                    $(".reset-pass-confirm").attr("data-confirm-for-email", inputVal);
                    $(".reset-pass-confirm").removeClass("d-none");
                }
            })
        }else{
            $(".reset-pass-email .validation-error-msg.d-none").removeClass("d-none");
        }
    });

    $(".popup-container").on("click", "#resetPassConfirmBtn", function(e){
        e.preventDefault();
        const $this = $(this);
        let input = $this.closest(".input-area").find("input");
        if(input.val().trim() != ""){
            $this.closest(".input-area").find(".validation-error-msg:not(.d-none)").addClass("d-none");
            $this.closest(".input-area").find("form").append(loader);
            $this.remove();
            let url = '/forgot/confirm-email';
            let email = $("#resetPasswordPopup .reset-pass-confirm").attr("data-confirm-for-email");
            let body = 'confirmcode='+input.val().trim()+'&email='+email;
            requestPost(url, body, function () {
                if (this.readyState == 4) {
                    let result = JSON.parse(this.responseText);
                    hideLoad();
                    if (!result.error) {
                        $(".reset-pass-confirm").addClass("d-none");
                        $(".new-password-area").removeClass("d-none");
                        $(".new-password-area").attr("data-token", result.token);
                    }else{
                        input.val("");
                        $(".reset-pass-confirm .validation-error-msg.d-none").removeClass("d-none");
                        $(".reset-pass-confirm .loader-area").remove();
                        $(".reset-pass-confirm form").append(resetPassConfirmBtn);
                    }
                }
            })
        }
        else{
            $(".reset-pass-confirm .validation-error-msg.d-none").removeClass("d-none");
        }
    });

    $(".popup-container").on("click", "#resetPassSaveBtn", function(e){
        e.preventDefault();
        const $this = $(this);
        let newPass = $("#setNewPass");
        $this.closest(".new-password-area").find(".validation-error-msg:not(.d-none)").addClass("d-none");
        if(newPass.val().trim() == ""){
            newPass.closest(".input-field").find(".validation-error-msg.d-none").removeClass("d-none");
        }
        else if(newPass.val().trim() != "" && ($("#confirmNewPass").val().trim() == "" || $("#confirmNewPass").val().trim() != newPass.val().trim())){
            $("#confirmNewPass .validation-error-msg.d-none").removeClass("d-none");
        }
        else if(newPass.val().trim() != "" && $("#confirmNewPass").val().trim() == newPass.val().trim()){
            $this.closest(".input-area").find(".validation-error-msg:not(.d-none)").addClass("d-none");
            $this.closest(".input-area").find("form").append(loader);
            $this.remove();
            let url = '/forgot/new-password';
            let token = $("#resetPasswordPopup .new-password-area").attr("data-token");
            let body = 'token='+token+'&password='+newPass.val().trim()+'&repassword='+$("#confirmNewPass").val().trim();
            requestPost(url, body, function () {
                if (this.readyState == 4) {
                    let result = JSON.parse(this.responseText);
                    hideLoad();
                    if (!result.error) {
                        $(".new-password-area").addClass("d-none");
                        $("#resetPasswordPopup").removeClass("active").fadeOut(300);
                        $(".reset-pass-email").addClass("d-none");
                        if($(".reset-pass-email .loader-area").length > 0){
                            $(".reset-pass-email .loader-area").remove();
                            $(".reset-pass-email form").append(resetPassBtn);
                        }
                        if($(".reset-pass-confirm .loader-area").length > 0){
                            $(".reset-pass-confirm .loader-area").remove();
                            $(".reset-pass-confirm form").append(resetPassConfirmBtn);
                        }
                        if($(".new-password-area .loader-area").length > 0){
                            $(".new-password-area .loader-area").remove();
                            $(".new-password-area form").append(resetPassSaveBtn);
                        }
                        setTimeout(function(){
                            $(".popup-outer .tab-item[data-tab='signin']").addClass("active");
                            $(".popup-outer .tab-link[data-tab='signin']").addClass("active");
                            $(".popup-outer").fadeIn(300).addClass("active");
                        },300)
                    }else{
                        if(result.msg === "80203"){
                            $("#confirmNewPass .min-error-msg.d-none").removeClass("d-none");
                            $(".new-password-area .loader-area").remove();
                            $(".new-password-area form").append(resetPassSaveBtn);
                        }
                        else{
                            $("#confirmNewPass .validation-error-msg.d-none").removeClass("d-none");
                            $(".new-password-area .loader-area").remove();
                            $(".new-password-area form").append(resetPassSaveBtn);
                        }
                    }
                }
            })
        }
    })

    $(document).on("click", "#resetPasswordPopup.active", function(e){
        if($(e.target).closest(".popup-container").length === 0){
            $("#resetPasswordPopup").fadeOut(300).removeClass("active");
            setTimeout(function(){
                $("#resetPasswordPopup .input-area:not(.reset-pass-email.d-none)").addClass("d-none");
                $("#resetPasswordPopup .reset-pass-email.d-none").removeClass("d-none");
                $("#resetPasswordPopup .input-area input").val("");
                if($(".reset-pass-email .loader-area").length > 0){
                    $(".reset-pass-email .loader-area").remove();
                    $(".reset-pass-email form").append(resetPassBtn);
                }
                if($(".reset-pass-confirm .loader-area").length > 0){
                    $(".reset-pass-confirm .loader-area").remove();
                    $(".reset-pass-confirm form").append(resetPassConfirmBtn);
                }
                if($(".new-password-area .loader-area").length > 0){
                    $(".new-password-area .loader-area").remove();
                    $(".new-password-area form").append(resetPassSaveBtn);
                }
            },300)

        }
    });
}



$('.one-tournament-area .tab-header').on('click', 'a:not(.active)', function(){
    var $this = $(this);
    $('.one-tournament-area .tabs-area .tab-header a.active').removeClass('active');
    $this.addClass('active');
    $('.one-tournament-area .tabs-area .tab-content>.item.active').removeClass('active');
    $('.one-tournament-area .tabs-area .tab-content>.item[data-tab="' + $this.attr('data-tab') + '"]').addClass('active');
});
$(".rules-list .item .rule-tab a").click(function () {
    var $item = $(this).closest(".item");
    var $text = $item.find(".tab-content");
    if (!$item.hasClass("active")) {
        $(".rules-list .item.active .tab-content").slideUp(300);
        $(".rules-list .item.active").removeClass("active");
        $item.addClass("active");
        $text.slideDown(300);
    }
    else {
        $text.slideUp(300);
        $item.removeClass("active");
    }
});
