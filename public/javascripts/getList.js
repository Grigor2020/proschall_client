var allData = [];
var allConsoles = [];
var FILTRATION = {
    prize : 0,
    console : 0,
    page : 1
}

getChallengeList(FILTRATION.page);

$('.__load_more').click(function () {
    getChallengeList(FILTRATION.page);
})

function getChallengeList(page){
    // console.log('FILTRATION',FILTRATION)
    let sQu = window.location.pathname.split('/')
    let body = "game_url="+sQu[2]+"&page="+page+"&prize="+FILTRATION.prize+"&console="+FILTRATION.console;
    let url = "/games/get-list";
    requestPost(url,body,function () {
        if(this.readyState == 4){
            let result = JSON.parse(this.responseText);
            if(!result.error){
                allData = allData.concat(result.data.list);
                allConsoles = result.data.consoles;
                setList(allData);
                setConsolesList(result.data.consoles)
                setMinMaxPrices(result.data.prices)
                let nextPage = parseInt(page);
                FILTRATION.page = ++nextPage
                // $('.__load_more').attr('data-next-page',++nextPage);
                hideLoad();
            }else{
                alert('error')
            }

        }
    })
}


function setList(data) {
    var content = "";
    for(var i = 0; i < data.length; i++){
        var userImg = '<img src="/images/user-placeholder-men.svg" alt="User">';
        if(data[i].user_img !== null){
            userImg = '<img src="'+data[i].user_img+'" alt="User">';
        }
        content += `
            <div class="card">
                <div class="img-area">
                    <img src="/images/browse-match/card-header.svg" alt="">
                    <div class="console-logo">
                        <img src=${data[i].console_image} alt="">
                    </div>
                    <div class="versus-area">
                        ${data[i].game_img}
                        <span>${data[i].mode_name}</span>
                    </div>
                </div>
                <div class="txt-area">
                    <div class="user">
                        <div class="img-area">${userImg}</div>
                        <div class="txt-area">
                            <p class="name"><span>${translations.prizchall_username}: </span>${data[i].username}</p>
                            <p class="status"><span>${translations.game_username}: </span>${data[i].username_for_game}</p>
                        </div>
                    </div>
                    <div class="info">
                        <div class="row">
                            <div class="txt">
                                <span>${translations.game_mode}</span>
                                <p>${data[i].mode_name}</p>
                            </div>
                            <div class="txt">
                                <span>Prize</span>
                                <p>$${data[i].winner_priz}</p>
                            </div>
                        </div>
                        <div class="row">
                           
                        </div>
                        <div class="row">
                            <div class="txt">
                                <span>Entry</span>
                                <p>$${data[i].prize}</p>
                            </div>
                        </div>
                    </div>
                    <div class="button-area">
                        <a href="/challenge/${data[i].challenge_id}" class="join-btn card-btn btn-yellow">${translations.join_match}</a>
                    </div>
                </div>
            </div>`

        $('.cards-list').html(content)
    }
}

function setConsolesList(data) {
    var mainBlock = $('#game-check-consoles');
    var content = "";
    for(var i = 0; i < data.length; i++){
        content += "" +
            "<li><a href='javascript:;'>"+data[i].name+"</a></li>"
    }
    $(mainBlock).html(content)
}

function setMinMaxPrices(data) {
   // console.log('setMinMaxPrices....data',data)
}

$(document).on("click", function (e) {
    if ($(".filter-arae .filter-item .label").hasClass("active") && $(e.target).closest(".filter-arae .filter-item").length === 0 && $(e.target).closest(".filter-arae .filter-item ul").length === 0) {
        $(".filter-arae .filter-item ul.active").slideUp(300);
        $(".filter-arae .filter-item ul.active").removeClass("active");
        $(".filter-arae .filter-item .label").removeClass("active");
    }
});

$(document).ready(function () {
    $(".filter-arae .filter-item .label").click(function () {
        var $this = $(this);
        var $list = $this.closest(".filter-item").find("ul");
        if (!$this.hasClass("active")) {
            $(".filter-arae .filter-item ul.active").slideUp(300);
            $(".filter-arae .filter-item ul.active").removeClass("active");
            $(".filter-arae .filter-item .label").removeClass("active");
            $this.addClass("active");
            $list.addClass("active");
            $list.slideDown(300);
        }
        else {
            $this.removeClass("active");
            $list.slideUp(300);
            $list.removeClass("active");
        }
    });

    $(".filter-arae").on("click", "ul a[href='javascript:;']", function(){
        let $this = $(this);
        $this.closest("ul").find("a.active").removeClass("active");
        $this.addClass("active");
        var x = $this.closest(".filter-item").find(".label a").text($this.text());
        $this.closest(".filter-item").find(".label.active").removeClass("active");
        $this.closest("ul").slideUp(300).removeClass("active");

        switch ($this.text()) {
            case 'Max to Min' :
                FILTRATION.prize = 1
                FILTRATION.page = 1
                allData = []
                break
            case 'Min to Max' :
                FILTRATION.prize = 2
                FILTRATION.page = 1
                allData = []
                break
            case 'PlayStation':
                FILTRATION.console = getConsoleId('PlayStation')
                FILTRATION.page = 1
                allData = []
                break
            case 'PC':
                FILTRATION.console = getConsoleId('PC')
                FILTRATION.page = 1
                allData = []
                break
            case 'Xbox':
                FILTRATION.console = getConsoleId('Xbox')
                FILTRATION.page = 1
                allData = []
                break
            default :
                FILTRATION.prize = 0
                FILTRATION.console = 0
                allData = []
                break
        }

        setList(allData);
        getChallengeList(FILTRATION.page);
        // if($this.attr("data-gid") !== undefined){
        //     $(".cards-list .card.active").fadeOut(0).removeClass("active");
        //     $(".cards-list:not(.by-game)").addClass("by-game");
        //     if($(".cards-list.by-console").length == 0){
        //         $(`.cards-list .card[data-gid="${$this.attr("data-gid")}"]`).fadeIn(300).addClass("active");
        //     }
        //     else{
        //         $(`.cards-list .card[data-gid="${$this.attr("data-gid")}"][data-cid="${$("#filterConsoles a.active").attr("data-cid")}"]`).fadeIn(300).addClass("active");
        //     }
        // }
        // else
        // if($this.attr("data-cid") !== undefined){
        //     $(".cards-list .card.active").fadeOut(0).removeClass("active");
        //     $(".cards-list:not(.by-console)").addClass("by-console");
        //     if($(".cards-list.by-game").length == 0){
        //         $(`.cards-list .card[data-cid="${$this.attr("data-cid")}"]`).fadeIn(300).addClass("active");
        //     }
        //     else{
        //         $(`.cards-list .card[data-cid="${$this.attr("data-cid")}"][data-gid="${$("#filterGames a.active").attr("data-gid")}"]`).fadeIn(300).addClass("active");
        //     }
        // }
        // else if($this.attr("data-prize") !== undefined){
            // console.log('$this.attr("data-prize")',$this.attr("data-prize"))
        //     let arr = [];
        //     $(".cards-list .card.active").each(function(){
        //         let $this = $(this);
        //         let gid = $this.attr("data-id");
        //         let prize = parseFloat($this.attr("data-prize"));
        //         arr.push({gid : gid,prize: prize});
        //     });
        //     if($this.attr("data-prize") == "min-max"){
        //         arr.sort((a,b) => a.prize-b.prize);
        //     }
        //     else if($this.attr("data-prize") == "max-min"){
        //         arr.sort((a,b) => b.prize-a.prize);
        //     }
        //     $.each(arr, function(key, value){
        //         $(".card[data-id='" + value.gid + "']").css("order", key);
        //     })
        // }
    });

    function getConsoleId(name) {
        console.log('name',name)
        console.log('allConsoles',allConsoles)
        var thisId = null;
        allConsoles.forEach((console)=>{
            if(name === console.name){
                thisId = console.console_id
            }
        })
        return thisId
    }
})
// var arr = [];
// var models = $('#filter-make > option');
// for(var i = 0; i < models.length; i++){
//     arr.push($(models[i]).text());
// }
// console.log(arr);
