$(function(){
    var mainBox = $('.chat-box');
    // getMessages();
    // setInterval(function(){ getMessages() }, 20000);

    var arrow = $('.chat-head');
    var textarea = $('.chat-text textarea');
    var sendButton = $('#chat-send-img');
    $(".chat-body").animate({ scrollTop: $('.chat-body').prop("scrollHeight")}, 0);

    arrow.on('click', function(){
        var newCount = $('.chat-new-count').text()
        console.log('newCount',newCount)
        if(parseInt(newCount) > 0){
            setViewed(function (){

            })
        }
        var img = $('#chat-head-img');
        var src = img.attr('src');
        $('.chat-body').slideToggle('fast');
        if(src == '/images/chevron-down-solid.svg'){
            img.attr('src', '/images/chevron-up-solid.svg');
        }
        else{
            img.attr('src', '/images/chevron-down-solid.svg');
        }
    });

    sendButton.on('click',function() {
        var msg = textarea.val();
        if(msg.trim() !== ''){
            var body = 'msg='+msg;
            var url ='/auth/send-msgs';
            requestPostWithoutSpin(url,body,function (){
                if (this.readyState == 4) {
                    console.log('................/auth/send-msgs')
                    let result = JSON.parse(this.responseText);
                    console.log('result',result)
                    if (!result.error) {
                        textarea.val('');
                        $('.msg-insert').append("<div class='msg-send'>"+msg+"</div>");
                        $(".chat-body").animate({ scrollTop: $('.chat-body').prop("scrollHeight")}, 1000);
                        return  false
                    }else{
                        alert('error')
                    }
                }
            })
        }else {
            return  false
        }
    });

});

function getMessages(){
    requestPostWithoutSpin('/auth/get-messages','',function (){
        console.log('................/auth/get-messages')
        if (this.readyState == 4) {
            let result = JSON.parse(this.responseText);
            if (!result.error) {
                if(!result.error){
                    $('.msg-insert').html('');
                    let newCount = 0;
                    for(var i = 0; i < result.data.length; i++){
                        console.log('................result',result)
                        if(result.data[i].self_msg === '0'){
                            $('.msg-insert').append("<div class='msg-receive'>"+result.data[i].msg+"</div>");
                            if(result.data[i].view == '0'){
                                newCount++;
                            }
                        }else{
                            $('.msg-insert').append("<div class='msg-send'>"+result.data[i].msg+"</div>");
                        }
                    }
                    if(newCount > 0){
                        $('.chat-new-count').css({'display':'flex'})
                        $('.chat-new-count').text(newCount)
                    }
                    $(".chat-body").animate({ scrollTop: $('.chat-body').prop("scrollHeight")}, 0);
                }
            }
        }
    })
}

function setViewed(){
    requestPostWithoutSpin('/auth/set-msg-viewed','',function (){
        console.log('................/auth/set-msg-viewed')
        // mainBox.addClass('loader')
        if (this.readyState == 4) {
            let result = JSON.parse(this.responseText);
            if (!result.error) {
                console.log('result',result)
                if(!result.error){
                    $('.chat-new-count').css({'display':'none'})
                    $('.chat-new-count').text(0)
                }
            }
        }
    })
}
