var resultData = null;
const monthNames = ["January", "February", "March", "April", "May", "June",
    "July", "August", "September", "October", "November", "December"
];
function addZero(i) {
    if (i < 10) {
        i = "0" + i;
    }
    return i;
}

const url = window.location.href.split('/');

requestPost('/tournament/get-tournament','tid='+url[url.length - 1],function () {
    if (this.readyState == 4) {
        let result = JSON.parse(this.responseText);
        console.log('result',result)
        if(!result.error){
            resultData = result.data;
            setPageContent(resultData.tournament,resultData.isRegistered,resultData.actionType)
            setMembersContent(resultData.tournamentMembers);
            setBrackets(resultData.data,resultData.roundType);
            // setParticipateButton(resultData.actionType,resultData.tournament.tournamentId,resultData.isRegistered);
            checkTournamentTimes(resultData.tournament.tournamentCheckInStartDate,resultData.tournament.tournamentStartDate);

            hideLoad();
        }
        else{
            hideLoad();
            alert("error")
        }
    }
})

var checkTournamentTimes = (checkInDate,stDate)=>{
    setInterval(()=>{
        let checkIn = new Date(checkInDate)
        let startDate = new Date(stDate)
        let now = new Date();
        // console.log('resultData.data',resultData.data)
        // console.log('checkInDate',checkIn)
        // console.log('startDate',startDate)
        // console.log('now',now)
        if(now > checkIn){
            if(resultData.data.quarterFinals.groups.length === 0){
                console.log('Need get data')
                // window.location.reload();
            }
        }
    },5000)
}

// var setParticipateButton = (actionType,tournamentId,isRegistered)=>{
//     let content = ''
//     // console.log('actionType',actionType)
//     if(actionType === 1){
//         if(isRegistered){
//             content+=`<a data-id="${tournamentId}" href="javascript:;" class="owned"><img src="/images/chacked-icon.svg" alt=""> participate</a>`
//         }
//     }else if(actionType === 2){
//
//     }else{
//
//     }
//     $('#gTour-ent-buttons').append(content)
// }

var setBrackets = (data,roundType)=>{
    // console.log('roundType',roundType)
    // console.log('data',data)
    var content = '';
    if(roundType === 0) {
        setAllNullableBrackets();
    } else if(roundType === 1) {

        var checkGamesCount = 0;
        data.quarterFinals.groups.forEach((group) => {
            group.games.forEach((game) => {
                checkGamesCount++;
            });
        });
        if (checkGamesCount == 3) {
            data.quarterFinals.groups.forEach((group) => {
                group.games.forEach((game) => {
                    content += `<div class="item">`;
                    var usernames = game.usernames.split('||')
                    var members_id = game.members_id.split('||')
                    var winner = game.winner.split('||')
                    for (var i = 0; i < members_id.length; i++) {
                        content += `<p class="player">
                            <span class="name">${members_id[i]} - ${usernames[i]}</span>`
                        if (game.winner === '0') {
                            content += `<span class="score win"></span>`
                        } else {
                            if (winner[i] == '0') {
                                content += `<span class="score lost">&times;</span>`
                            } else {
                                content += `<span class="score win">&#10004;</span>`
                            }
                        }
                        content += `</p>`
                    }
                    content += `</div>`
                });
            })
            content += `<div class="item">
                                                    <p class="player">
                                                        <span class="name">No Player</span>
                                                    </p>
                                                    <p class="player">
                                                        <span class="name">No Player</span>
                                                    </p>
                                                </div>`;
        } else if (checkGamesCount == 2) {
            data.quarterFinals.groups.forEach((group) => {
                group.games.forEach((game) => {
                    content += `<div class="item">`;
                    var usernames = game.usernames.split('||')
                    var members_id = game.members_id.split('||')
                    var winner = game.winner.split('||')
                    for (var i = 0; i < members_id.length; i++) {
                        content += `<p class="player">
                            <span class="name">${members_id[i]} - ${usernames[i]}</span>`
                        if (game.winner === '0') {
                            content += `<span class="score win"></span>`
                        } else {
                            if (winner[i] == '0') {
                                content += `<span class="score lost">&times;</span>`
                            } else {
                                content += `<span class="score win">&#10004;</span>`
                            }
                        }
                        content += `</p>`
                    }
                    content += `</div>`
                });
            })
        }
        // console.log('checkGamesCount', checkGamesCount)
        setNullableFinals();
        setNullableSemiFinals();

        $('#quarterFinals').html(content)
    }
}

var setNullableFinals = ()=>{
    var finalsContent = `  <div class="item">
                                            <p class="player">
                                                <span class="name">----------</span>
                                                <span class="score lost"></span>
                                            </p>
                                            <p class="player">
                                                <span class="name">----------</span>
                                                <span class="score win"></span>
                                            </p>
                                        </div>`;
    $('#finals').html(finalsContent)
}
var setNullableSemiFinals = ()=>{
    var semifinalsContent = ` <div class="item">
                                            <p class="player">
                                                <span class="name">----------</span>
                                                <span class="score lost"></span>
                                            </p>
                                            <p class="player">
                                                <span class="name">----------</span>
                                                <span class="score win"></span>
                                            </p>
                                        </div>
                                        <div class="item">
                                            <p class="player">
                                                <span class="name">----------</span>
                                                <span class="score lost"></span>
                                            </p>
                                            <p class="player">
                                                <span class="name">----------</span>
                                                <span class="score win"></span>
                                            </p>
                                        </div>`
    $('#semiFinals').html(semifinalsContent)
}
var setNullableQuarterFilanls = ()=>{
    var quarterfinalsContent = ` <div class="item">
                                            <p class="player">
                                                <span class="name">----------</span>
                                                <span class="score lost"></span>
                                            </p>
                                            <p class="player">
                                                <span class="name">----------</span>
                                                <span class="score win"></span>
                                            </p>
                                        </div>
                                        <div class="item">
                                            <p class="player">
                                                <span class="name">----------</span>
                                                <span class="score lost"></span>
                                            </p>
                                            <p class="player">
                                                <span class="name">----------</span>
                                                <span class="score win"></span>
                                            </p>
                                        </div>
                                        <div class="item">
                                            <p class="player">
                                                <span class="name">----------</span>
                                                <span class="score lost"></span>
                                            </p>
                                            <p class="player">
                                                <span class="name">----------</span>
                                                <span class="score win"></span>
                                            </p>
                                        </div>
                                        <div class="item">
                                            <p class="player">
                                                <span class="name">----------</span>
                                                <span class="score lost"></span>
                                            </p>
                                            <p class="player">
                                                <span class="name">----------</span>
                                                <span class="score win"></span>
                                            </p>
                                        </div>`
    $('#quarterFinals').html(quarterfinalsContent)
}

var setAllNullableBrackets = () =>{
    setNullableQuarterFilanls();
    setNullableSemiFinals();
    setNullableFinals();
}
var setPageContent = (data,isRegistered,actionType)=>{
    var startDate = new Date(data.tournamentStartDate)
    var checkInDate = new Date(data.tournamentCheckInStartDate)

    $('#gTour-name').text(data.tournamentName)
    $('#gTour-sec-name').text(data.tournamentName)
    $('#gTour-console').text(data.consolesName)
    $('#gTour-prize').text('$ '+ data.tournamentPrize_1)
    $('#mgTour-prize').text('total prize pool: $ '+ data.tournamentPrize_1)
    $('#mgTour-reg-count').text('participants: '+ data.registeredUsersCount)
    $('#gTour-first-place').text('$ '+ data.tournamentPrize_1)
    $('#gTour-second-place').text('$ '+ data.tournamentPrize_2)
    $('#gTour-entry').text(data.tournamentEntry)
    $('#gTour-mode').text(data.gameModeName)
    $('#gTour-game').text(data.gameName)
    $('#gTour-ent-fee').text('entery fee : '+ (8-parseInt(data.registeredUsersCount)))
    $('#gTour-start-date').text(startDate.getDate() + ' - '+  monthNames[startDate.getMonth()] +' - '+ startDate.getFullYear()+' '+ addZero(startDate.getHours())+':'+addZero(startDate.getMinutes()))
    $('#mgTour-start-date').text(startDate.getDate() + ' - '+  monthNames[startDate.getMonth()] +' - '+ startDate.getFullYear()+' '+ addZero(startDate.getHours())+':'+addZero(startDate.getMinutes()))
    $('#gTour-checkin-date').text(checkInDate.getDate() + ' - '+  monthNames[checkInDate.getMonth()] +' - '+ checkInDate.getFullYear()+' '+ addZero(checkInDate.getHours())+':'+addZero(checkInDate.getMinutes()))

    let buttonContent = '';
    if(data.registeredUsersCount < 8){
        if(actionType === 1){
            if(isRegistered === false){
                buttonContent = `<a data-id="${data.tournamentId}" href="javascript:;" class="owned"><img src="/images/chacked-icon.svg" alt=""> participate</a>`
            }else{
                buttonContent = `<a data-id="${data.tournamentId}" href="javascript:;" class=""><img src="/images/chacked-icon.svg" alt="">unjoin</a>`
            }
        }

    }
    $('#gTour-parts').html(buttonContent)
}

var setMembersContent = (members)=>{
    let membersContent = '';
    for(let i = 0; i < members.length; i++){
        membersContent+= ` 
                    <div class="row">
                        <span class="name">${i+1}</span><span class="value"> ${members[i].username}</span>
                    </div>`
    }
    $('#gTour-members').html(membersContent)
}

$(document).on('click','.owned', function () {
    requestPost('/tournament/join','tournament_id='+$(this).attr('data-id')+'',function () {
        if (this.readyState == 4) {
            let result = JSON.parse(this.responseText);
            // console.log('result',result)
            if(!result.error){
                window.location.reload();
            }else{
                if(result.msg == '45695'){
                    var loginPopup =  $('#loginPopUp')
                    loginPopup.css({'display' : 'block'})
                    loginPopup.addClass('active')
                    $(".popup-outer .tab-item[data-tab='signin']").addClass("active");
                    $(".popup-outer .tab-link[data-tab='signin']").addClass("active");
                }else if(result.msg == '42005'){
                    $("#noMoneyErrorPopup").addClass("active").fadeIn(300);
                    setTimeout(function(){
                        $("#noMoneyErrorPopup").fadeOut(300).removeClass("active");
                    }, 2000)
                }else if(result.msg == '42001'){
                    alert('you are conected')
                }
            }
        }
        hideLoad()
    })
})