//
if(typeof userInfo.email !== "undefined"){
    // console.log('userInfo.email',userInfo.email)
    setNotes(0,notes,translations, function (data) {
        // console.log('data',data)
        $('.notifications .n-list').html(data)
    })

    setInterval(function () {
        getNotes();
    },30000)

}
// setTimeout(function () {
//
// })
function getNotes() {
    var url = '/notification/get-all';
    requestPostWithoutSpin(url,'',function () {
        if(this.readyState == 4){
            let result = JSON.parse(this.responseText);
            if(!result.error) {
                // console.log('result',result)
                // TODO: open below 4 lines before merge
                setNotes(1,result.notes,translations, function (data) {
                    // console.log('data',data)
                    $('.notifications .n-list').html(data)
                })
                setCount(result.unreadCount)
            }else{

            }
        }
    })
}

function setCount(count) {
    if(count == '0'){
        $('.notification-count').addClass('d-none')
    }else{
        $('.notification-count').removeClass('d-none')
        $('.notification-count').text(count)
        $('#notification_reminder').addClass('notify_active');
        setTimeout(()=>{
            $('#notification_reminder').removeClass('notify_active');
        },2000)
    }
}

function setNotes(type,notes,translations, cb) {
    if(type === 1){
        $('.notifications .n-list').html('')
    }
    var content = "";
    var languages = translations
    // console.log('lang',lang)
    let style = ""
    if(lang === 'en'){
        style = "right : 100%;"
    }else{
        style = "left :100%;"
    }
    notes.forEach(function (data) {
        let  unreadClassName = "";
        if(data.view == '0'){
            unreadClassName = ' unread'
        }
        if(data.type == '0'){
            content += `
        <div data-notify-id='${data.notificationId}' class='item'>
            <div class='txt-area'>
                <p>
                    <span class='user'>
                        <a href='javascript:;'>${data.s_user_username}</a>
                    </span> ${languages.notification_0_1} 
                </p>
            </div>
            <div class='status-area'>
                <span class='status${unreadClassName}'></span>
            </div>
            <div class='notification' style="${style}">
<!--                    <div class='header-txt'>-->
<!--                        <span class='from-txt'>From: Prizchall</span>-->
<!--                        <span class='date'>28 August 2019 4:00:01</span>-->
<!--                    </div>-->
                <div class='n-content'>
                    <p>
                        <span class='user'>
                            <a href='javascript:;'>${data.s_user_username}</a>
                        </span> ${languages.notification_0_1} 
                    </p>
                </div>
                <div class='buttons-area'>
                    <a href='javascript:;' class='notify-btn btn-yellow' data-match-id='${data.matchesId}' data-challenge-id='${data.challenge_id}' data-notify-type="${data.type}" data-status='1'>${languages.notification_start} </a>
                    <a href='javascript:;' class='cancel-btn' data-match-id='${data.matchesId}' data-challenge-id='${data.challenge_id}' data-status='0'>${languages.notification_decline}</a>
                </div>
            </div>
        </div>`;
        }
        else if(data.type == '1'){
            content += `
        <div class='item' data-notify-id='${data.notificationId}'>
            <div class='txt-area'>
                <p>${languages.notification_welcome} </p>
            </div>
            <div class='status-area'>
                <span class='status${unreadClassName}'></span>
            </div>
        </div>`;
        }
        else if(data.type == '2'){
            content += `
        <div class='item' data-notify-id='${data.notificationId}'>
            <div class='txt-area'>
                <p>${languages.notification_2} </p>
            </div>
            <div class='status-area'>
                <span class='status${unreadClassName}'></span>
            </div>
            <div class='notification' style="${style}">
<!--                    <div class='header-txt'>-->
<!--                        <span class='from-txt'>From: Prizchall</span>-->
<!--                        <span class='date'>28 August 2019 4:00:01</span>-->
<!--                    </div>-->
                <div class='n-content'>
                    <p>${languages.notification_2}</p>
                </div>
                <div class='buttons-area'>
                    <a href='/profile' class='notify-btn btn-yellow'>${languages.notification_set_usernames}</a>
                    <a href='javascript:;' class='cancel-btn'>${languages.notification_decline}</a>
                </div>
            </div>
        </div>`;
        }
        else if(data.type == '3'){
            content += `
        <div class='item' data-notify-id='${data.notificationId}'>
            <div class='txt-area'>
                <p>${languages.notification_3}</p>
            </div>
            <div class='status-area'>
                <span class='status${unreadClassName}'></span>
            </div>
        </div>`;
        }
        else if(data.type == '4'){
            content += `
        <div class='item' data-notify-id='${data.notificationId}'>
            <div class='txt-area'>
                <p>
                    <span class='user'>
                        <a href='javascript:;'>${data.s_user_username}</a>
                    </span>${languages.notification_4}
                </p>
            </div>
            <div class='status-area'>
                <span class='status${unreadClassName}'></span>
            </div>
            <div class='notification' style="${style}">
<!--                    <div class='header-txt'>-->
<!--                        <span class='from-txt'>From: Prizchall</span>-->
<!--                        <span class='date'>28 August 2019 4:00:01</span>-->
<!--                    </div>-->
                <div class='n-content'>
                    <p>
                        <span class='user'>
                            <a href='javascript:;'>${data.s_user_username}</a>
                        </span> ${languages.notification_4} <span class='user'><a href='/match/${data.matchesId}'>#${data.matchesId}</a></span>.
                    </p>
                </div>
                <div class='buttons-area'>
                    <a href='javascript:;' data-notify-type="${data.type}" class='notify-btn btn-yellow' data-match-id='${data.matchesId }' data-challenge-id='${data.challenge_id}' data-status='1'>${languages.notification_accept}</a>
                    <a href='javascript:;' data-notify-type="${data.type}" class='cancel-btn' data-match-id='${data.matchesId }' data-challenge-id='${data.challenge_id}' data-status='0'>${languages.notification_decline}</a>
                </div>
            </div>
        </div>`;
        }
        else if(data.type == '5'){
            content += `
        <div class='item' data-notify-id='${data.notificationId}'>
            <div class='txt-area'>
                <p>${languages.notification_5} </p>
            </div>
            <div class='status-area'>
                <span class='status${unreadClassName}'></span>
            </div>
        </div>`;
        }
        else if(data.type == '6'){
            content += `
        <div class='item' data-notify-id='${data.notificationId}'>
            <div class='txt-area'>
                <p>${languages.notification_6}</p>
            </div>
            <div class='status-area'>
                <span class='status${unreadClassName}'></span>
            </div>
        </div>`;
        }
        else if(data.type == '7'){
            content += `
        <div class='item' data-notify-id='${data.notificationId}'>
            <div class='txt-area'>
                <p>${languages.notification_7}</p>
            </div>
            <div class='status-area'>
                <span class='status${unreadClassName}'></span>
            </div>
            <div class='notification' style="${style}">
<!--                    <div class='header-txt'>-->
<!--                        <span class='from-txt'>From: Prizchall</span>-->
<!--                        <span class='date'>28 August 2019 4:00:01</span>-->
<!--                    </div>-->
                <div class='n-content'>
                    <p>${languages.notification_7} <a href='/match/${data.matchesId}'>#${data.matchesId}</a>.</p>
                </div>
                <div class='buttons-area'>
                    <a href='javascript:;' class='notify-btn btn-yellow' data-match-id='${data.matchesId}' data-challenge-id='${data.challenge_id}' data-notify-type="${data.type}" data-status='1'>${languages.notification_start}</a>
                    <a href='javascript:;' class='cancel-btn' data-match-id='${data.matchesId}' data-challenge-id='${data.challenge_id}' data-status='0'>${languages.notification_decline}</a>
                </div>
            </div>
        </div>`;
        }
        else if(data.type == '8'){
            content += `
        <div class='item' data-notify-id='${data.notificationId}'>
            <div class='txt-area'>
                <p>${languages.match} <span class='user'><a href='/match/${data.matchesId}'>#${data.matchesId}</a></span><span class=""></span> ${languages.is_started}</p>
            </div>
            <div class='status-area'>
                <span class='status${unreadClassName}'></span>
            </div>
        </div>`;
        }
        else if(data.type == '9'){
            content += `
            <div class='item' data-notify-id='${data.notificationId}'>
                <div class='txt-area'>
                    <p> ${languages.notification_9}</p>
                </div>
                <div class='status-area'>
                    <span class='status${unreadClassName}'></span>
                </div>
            </div>`;
        }
        else if(data.type == '10'){
            content += `
        <div class='item' data-notify-id='${data.notificationId}'>
            <div class='txt-area'>
                <p>
                    <span class='user'>
                        <a href='javascript:;'>${data.s_user_username}</a>
                    </span>  ${languages.has_won}
                </p>
            </div>
            <div class='status-area'>
                <span class='status${unreadClassName}'></span>
            </div>
            <div class='notification' style="${style}">
<!--                    <div class='header-txt'>-->
<!--                        <span class='from-txt'>From: Prizchall</span>-->
<!--                        <span class='date'>28 August 2019 4:00:01</span>-->
<!--                    </div>-->
                <div class='n-content'>
                    <p>
                        <span class='user'>
                            <a href='javascript:;'>${data.s_user_username}</a>
                        </span>  ${languages.has_won}
                    </p>
                </div>
                <div class='buttons-area'>
                    <a href='javascript:;' class='notify-btn btn-yellow' data-match-id='${data.matchesId }' data-challenge-id='${data.challenge_id}' data-status='1'>  ${languages.notification_accept}</a>
                    <a href='javascript:;' class='cancel-btn' data-match-id='${data.matchesId }' data-challenge-id='${data.challenge_id}' data-status='0' id='btnDispute'>  ${languages.open_dispute}</a>
                </div>
            </div>
        </div>`;
        }
        else if(data.type == '11'){
            content += `
        <div class='item' data-notify-id='${data.notificationId}'>
            <div class='txt-area'>
                <p> ${languages.notification_11}</p>
            </div>
            <div class='status-area'>
                <span class='status${unreadClassName}'></span>
            </div>
        </div>`;
        }
        else if(data.type == '12'){
            content += `
        <div class='item' data-notify-id='${data.notificationId}'>
            <div class='txt-area'>
                <p> ${languages.notification_12}</p>
            </div>
            <div class='status-area'>
                <span class='status${unreadClassName}'></span>
            </div>
        </div>`;
        }
        else if(data.type == '13'){
            content += `
        <div class='item' data-notify-id='${data.notificationId}'>
            <div class='txt-area'>
                <p> ${languages.notification_13}  ${data.money}$.</p>
            </div>
            <div class='status-area'>
                <span class='status${unreadClassName}'></span>
            </div>
        </div>`;
        }
        else if(data.type == '14'){
            content += `
        <div class='item' data-notify-id='${data.notificationId}'>
            <div class='txt-area'>
                <p>${languages.notification_14}</p>
            </div>
            <div class='status-area'>
                <span class='status${unreadClassName}'></span>
            </div>
            <div class='notification' style="${style}">
<!--                    <div class='header-txt'>-->
<!--                        <span class='from-txt'>From: Prizchall</span>-->
<!--                        <span class='date'>28 August 2019 4:00:01</span>-->
<!--                    </div>-->
                <div class='n-content'>
                    <p>${languages.notification_14}</p>
                </div>
            </div>
        </div>`;
        }
        else if(data.type == '15'){
            content += `
        <div class='item' data-notify-id='${data.notificationId}'>
            <div class='txt-area'>
                <p>${languages.notification_15x1}</p>
            </div>
            <div class='status-area'>
                <span class='status${unreadClassName}'></span>
            </div>
            <div class='notification' style="${style}">
<!--                    <div class='header-txt'>-->
<!--                        <span class='from-txt'>From: Prizchall</span>-->
<!--                        <span class='date'>28 August 2019 4:00:01</span>-->
<!--                    </div>-->
                <div class='n-content'>
                    <p>${languages.notification_15x2}</p>
                </div>
                <div class='buttons-area'>
                    <a href='/profile/detail' class='notify-btn btn-yellow'>${languages.notification_15x3}</a>
                </div>
            </div>
        </div>`;
        }
        else if(data.type == '16'){
            content += `
        <div class='item' data-notify-id='${data.notificationId}'>
            <div class='txt-area'>
                <p>${languages.notification_16}</p>
            </div>
            <div class='status-area'>
                <span class='status${unreadClassName}'></span>
            </div>
            <div class='notification' style="${style}">
<!--                    <div class='header-txt'>-->
<!--                        <span class='from-txt'>From: Prizchall</span>-->
<!--                        <span class='date'>28 August 2019 4:00:01</span>-->
<!--                    </div>-->
                <div class='n-content'>
                    <p>${languages.notification_16}</p>
                </div>
            </div>
        </div>`;
        }
        else if(data.type == '17'){
            content += `
        <div class='item' data-notify-id='${data.notificationId}'>
            <div class='txt-area'>
                <p>${languages.notification_17}</p>
            </div>
            <div class='status-area'>
                <span class='status${unreadClassName}'></span>
            </div>
        </div>`;
        }
        else if(data.type == '18'){
            content += `
        <div class='item' data-notify-id='${data.notificationId}'>
            <div class='txt-area'>
                <p>${languages.notification_18}</p>
            </div>
            <div class='status-area'>
                <span class='status${unreadClassName}'></span>
            </div>
        </div>`;
        }
        else if(data.type == '19'){
            content += `
        <div class='item' data-notify-id='${data.notificationId}'>
            <div class='txt-area'>
                <p>${languages.notification_19} <span class='user'><a href='/match/${data.matchesId}'>#${data.matchesId}</a></span><span class=""></span></p>
            </div>
            <div class='status-area'>
                <span class='status${unreadClassName}'></span>
            </div>
        </div>`;
        }
    })
    cb(content)
}
