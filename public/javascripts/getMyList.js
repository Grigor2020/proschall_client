var allData = [];
getMyList();

function getMyList(){
    let url = "/challenge/get-my-data";
    requestPost(url,{},function () {
        if(this.readyState == 4){
            let result = JSON.parse(this.responseText);
            if(!result.error){
                setMyList(result.data)
                setConsolesList(result.consoles);
                setGamesList(result.games);
                hideLoad();
            }else{

            }

        }
    })
}

function setMyList(data) {
    var content = "";
    for(var i = 0; i < data.length; i++){
        var userImg = '<img src="/images/user-placeholder-men.svg" alt="User">';
        if(data[i].user_img !== null){
            userImg = '<img src="'+data[i].user_img+'" alt="User">';
        }
        content +=
        `
            <div class="card active" data-id="${data[i].challenge_id}" data-prize="${data[i].prize}" data-gid="${data[i].game_id}" data-cid="${data[i].console_id}">
                <div class="img-area">
                    <img src="/images/browse-match/card-header.svg" alt="">
                    <div class="console-logo">
                        <img src="${data[i].console_img}" alt="${data[i].console_name}">
                    </div>
                   ${ data[i].is_ongoing === true ? `<div class="game-status green">Ongoing Match</div>`  : ``}
                    <div class="versus-area">
                        ${data[i].game_image}
                        <span>${data[i].mode_name}</span>
                    </div>
                </div>
                <div class="txt-area">
                    <div class="user">
                        <div class="img-area">
                            ${userImg}
                        </div>
                        <div class="txt-area">
                            <p class="name"><span>${translations.prizchall_username}: </span>${data[i].username}</p>
                            <p class="status"><span>${translations.game_username}: </span>${data[i].username_for_game}</p>
                        </div>
                    </div>
                    <div class="info">
                        <div class="row">
                            <div class="txt">
                                <span>${translations.game_mode}</span>
                                <p>${data[i].mode_name}</p>
                            </div>
                            <div class="txt">
                                <span>Prize</span>
                                <p>$${data[i].winner_priz}</p>
                            </div>
                            <!-- <div class="txt">
                                <span>Starts at</span>
                                <p>Aug 27, 5:30 PM</p>
                            </div> -->
                        </div>
                        <div class="row">
                           <div class="txt">
                                <span>Entry</span>
                                <p>$${data[i].prize}</p>
                            </div>
                        </div>
                    </div>
                    <div class="button-area">
                        <a href="/challenge/my/${data[i].challenge_id}" class="join-btn card-btn btn-yellow">
                            ${translations.my_match_details}
                        </a>
                    </div>
                </div>
            </div>
        `
        $('.cards-list').html(content)
    }
}

function setGamesList(data){
    var gamesList = $("#filterGames");
    var games = "";
    for(var j = 0; j < data.length; j++){
        games += `<li><a href='javascript:;' data-gid='${data[j].id}'>${data[j].name}</a></li>`
    }
    $(gamesList).html(games);
}
function setConsolesList(data) {
    var mainBlock = $('#filterConsoles');
    var content = "";
    for(var i = 0; i < data.length; i++){
        content += `<li><a href='javascript:;' data-cid='${data[i].id}'>${data[i].name}</a></li>`
    }
    $(mainBlock).html(content)
}

function setMinMaxPrices(data) {
    console.log('setMinMaxPrices....data',data)
}
