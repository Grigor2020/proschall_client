var setNotificationContent = require('./setNotificationContent');


var detectLanguageId = function(lang_code,allLanguages){
    var lang_id = '1';
    for(var i = 0; i < allLanguages.length; i++){
        if(allLanguages[i].lang_code == lang_code){
            lang_id = allLanguages[i].id
        }
    }

    return lang_id;
}


var getDateInUnix = function(date,time){
    var sDate = date.split('-');
    var tari = parseInt(sDate[0]);
    var amis = parseInt(sDate[1]);
    var or = parseInt(sDate[2]);

    var sTime = time.split(":");
    var jam = parseInt(sTime[0]);
    var rope = parseInt(sTime[1]);

    var datum = new Date(Date.UTC(tari,amis-1,or,jam,rope,0));
    return datum.getTime()/1000;
};

function generateUrlFrmoString(string) {
    const a = 'àáäâãåăæąçćčđďèéěėëêęğǵḧìíïîįłḿǹńňñòóöôœøṕŕřßşśšșťțùúüûǘůűūųẃẍÿýźžż·/_,:;'
    const b = 'aaaaaaaaacccddeeeeeeegghiiiiilmnnnnooooooprrsssssttuuuuuuuuuwxyyzzz------'
    const p = new RegExp(a.split('').join('|'), 'g')

    return string.toString().toLowerCase()
        .replace(/\s+/g, '-') // Replace spaces with -
        .replace(p, c => b.charAt(a.indexOf(c))) // Replace special characters
        .replace(/&/g, '-and-') // Replace & with 'and'
        .replace(/[^\w\-]+/g, '') // Remove all non-word characters
        .replace(/\-\-+/g, '-') // Replace multiple - with single -
        .replace(/^-+/, '') // Trim - from start of text
        .replace(/-+$/, '') // Trim - from end of text
}

function byNote(notes,lang){
    var contArr = [];
    for(var i = 0; i < notes.length; i++){
        contArr.push(setNotificationContent.setNotificationContent(notes[i],lang))
    }
    return contArr;
}
module.exports.detectLanguageId = detectLanguageId;
module.exports.getDateInUnix = getDateInUnix;
module.exports.generateUrlFrmoString = generateUrlFrmoString;
module.exports.byNote = byNote;
