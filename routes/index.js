var express = require('express');
var router = express.Router();
var statics = require('../static');
const request = require('request');
var staticMethods = require('../model/staticMethods');

/* GET home page. */
router.get('/', function (req, res, next) {
    var lang_id = staticMethods.detectLanguageId(req.siteData.lang,req.siteData.allLanguages);
    const options = {
        url: ''+statics.API_URL+'/faq/all/',
        method : "POST",
        headers: {
            'authorization': statics.API_AUTH
        },
        form: {
            lang_id : lang_id
        }
    };
    request(options, function (error, response, body) {
        if (!error && response.statusCode == 200) {
            const result = JSON.parse(body);
			// console.log('...faq',result.data);
            res.render('index',
                {
                    siteData: req.siteData,
                    faq : typeof result.data == "undefined" ? [] : result.data
                });
        }
    });

});





// router.get('/browse-game-matches', function (req, res, next) {
//   res.render('browseGameMatches',{
//       siteData: req.siteData
//   });
// });
// router.get('/match-details', function (req, res, next) {
//   res.render('matchDetails', {
//       siteData: req.siteData
//   });
// });
//
// router.get('/my-match-details', function (req, res, next) {
//   res.render('mymatchDetails', {
//       siteData: req.siteData
//   });
// });
//
// router.get('/history', function (req, res, next) {
//   res.render('history', {
//       siteData: req.siteData
//   });
// });
// router.get('/history-match-detail', function (req, res, next) {
//   res.render('historyMatchDetail', {
//       siteData: req.siteData
//   });
// });
//
// router.get('/account-security', function (req, res, next) {
//   res.render('accountSecurity', {
//       siteData: req.siteData
//   });
// });
//
// router.get('/deposit', function (req, res, next) {
//   res.render('deposit', {
//       siteData: req.siteData
//   });
// });
module.exports = router;
