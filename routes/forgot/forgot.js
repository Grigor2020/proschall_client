var express = require('express');
var router = express.Router();
var statics = require('../../static');
const request = require('request');

router.post('/send-email',function (req,res,next) {
    console.log('req.body',req.body);
    if(typeof req.body.email !== "undefined"){
        const options = {
            url: ''+statics.API_URL+'/forgot/check',
            method : "POST",
            headers: {
                'authorization': statics.API_AUTH
            },
            form: {
                email : req.body.email
            }
        };
        request(options, function (error, response, body) {
            if (!error && response.statusCode == 200) {
                const result = JSON.parse(body);
                if(!result.error){
                    res.json({error:false});
                }else {
                    res.json(result)
                }
            }
        });
    }else{
        res.json({error:true})
    }

});

router.post('/confirm-email',function (req,res,next) {
    if(typeof req.body.confirmcode !== "undefined" && typeof req.body.email !== "undefined"){
        const options = {
            url: ''+statics.API_URL+'/forgot/code',
            method : "POST",
            headers: {
                'authorization': statics.API_AUTH
            },
            form: {
                email : req.body.email,
                code : req.body.confirmcode
            }
        };
        request(options, function (error, response, body) {
            if (!error && response.statusCode == 200) {
                const result = JSON.parse(body);
                if(!result.error){
                    res.json(result);
                }else {
                    res.json(result)
                }
            }
        });
    }else{
        res.json({error:true})
    }
})

router.post('/new-password',function (req,res,next) {
    if(typeof req.body.token !== "undefined" && typeof req.body.password !== "undefined" && typeof req.body.repassword !== "undefined"){
        const options = {
            url: ''+statics.API_URL+'/forgot/set-password',
            method : "POST",
            headers: {
                'authorization': statics.API_AUTH
            },
            form: {
                token : req.body.token,
                password : req.body.password,
                repassword : req.body.repassword
            }
        };
        request(options, function (error, response, body) {
            if (!error && response.statusCode == 200) {
                const result = JSON.parse(body);
                if(!result.error){
                    res.json({error:false});
                }else {
                    res.json(result)
                }
            }
        });
    }else{
        res.json({error:true})
    }
})

module.exports = router;
