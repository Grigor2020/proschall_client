var express = require('express');
var router = express.Router();
var statics = require('../../static');
const request = require('request');

/* GET home page. */
// router.get('/all', function (req, res, next) {
//     res.render('games/games',
//         {
//             lang:req.lang,
//             allLanguages:req.allLanguages,
//             userInfo : req.userInfo,
//             allGames:req.allGames
//         });
// });

router.get('/:game_url', function (req, res, next) {
    // const options = {
    //     url: ''+statics.API_URL+'/challenge/get-list',
    //     method : "POST",
    //     headers: {
    //         'authorization': statics.API_AUTH
    //     },
    //     form: {
    //         game_url : req.params.game_url,
    //         page : 1
    //     }
    // };
    // request(options, function (error, response, body) {
    //
    //     if (!error && response.statusCode == 200) {
    //         const result = JSON.parse(body);
    //         if(!result.error){
                res.render('games/games',
                    {
                        siteData: req.siteData,
                        // data:result.data
                    });
            // }
        // }
    // });

});

router.post('/get-list',function (req,res,next) {
    if(typeof req.body.page !== "undefined" && typeof req.body.game_url !== "undefined"){
        const options = {
            url: ''+statics.API_URL+'/challenge/get-list',
            method : "POST",
            headers: {
                'authorization': statics.API_AUTH
            },
            form: {
                game_url : req.body.game_url,
                page : req.body.page,
                prize : req.body.prize,
                console : req.body.console
            }
        };
        if(typeof req.siteData.userInfo !== "undefined"){
            options.headers.void = req.siteData.userInfo.token
        }
        request(options, function (error, response, body) {
            if (!error && response.statusCode == 200) {
                const result = JSON.parse(body);
                if(!result.error){
                    res.json({error:false,data:result.data});
                }else {
                    res.json(result)
                }
            }
        });
    }else{
        res.json({error:true})
    }

})

module.exports = router;
