var express = require('express');
var router = express.Router();
var statics = require('../../static');
var request = require('request');
var staticMethods = require('../../model/staticMethods');
var checkUser = require('../../object/auth/auth');

router.get('/create',checkUser.checkUserAuth(), function (req, res, next) {
    res.render('challenge/create_challenge',
        {
            siteData: req.siteData
        });
});

router.get('/my/:id',checkUser.checkUserAuth(), function (req, res, next) {
        const options = {
            url: statics.API_URL + '/challenge/get-one-challenge',
            method: "POST",
            headers: {
                authorization: statics.API_AUTH
            },
            form: {
                "challenge_id": req.params.id
            }
        };
        request(options, function (error, response, body) {
            if (!error && response.statusCode == 200) {
                const result = JSON.parse(body);
                if (!result.error) {
                    res.render('challenge/my_challenge_detail',
                        {
                            siteData: req.siteData,
                            data: result.data[0]
                        });
                }
            }
        });
});

router.get('/my',checkUser.checkUserAuth(), function (req, res, next) {
    res.render('challenge/my_challenges',
        {
            siteData: req.siteData,
        });
});

router.get('/history',checkUser.checkUserAuth(), function (req, res, next) {
    res.render('match/my_match_history',
        {
            siteData: req.siteData,
        });
});

router.post('/get-my-data',checkUser.checkUserAuth(), function (req, res, next) {
    const options = {
        url: '' + statics.API_URL + '/challenge/get-my',
        method: "POST",
        headers: {
            authorization: statics.API_AUTH,
            void : req.siteData.userInfo.token
        },
        form : {
            langId : staticMethods.detectLanguageId(req.siteData.lang,req.siteData.allLanguages)
        }
    };
    request(options, function (error, response, body) {
        if (!error && response.statusCode == 200) {
            const result = JSON.parse(body);
            if (!result.error) {
                res.json(result);

            }
        }
    });

});

router.post('/get_game_info', function (req, res, next) {

    const options = {
        url: '' + statics.API_URL + '/challenge/get_game_info',
        method: "POST",
        headers: {
            authorization: statics.API_AUTH
        },
        form: {
            gameId: req.body.gameId
        }
    };
    request(options, function (error, response, body) {
        if (!error && response.statusCode == 200) {
            const result = JSON.parse(body);
            if (!result.error) {
                res.json({ error: false, data: result.data })
            }
        }
    });
});

router.post('/create',checkUser.checkUserAuth(), function (req, res, next) {
    let prize = req.body.prize;
    // let isNum = /.-\d+$/.test(prize);
    // if (isNum) {
        const options = {
            url: '' + statics.API_URL + '/challenge/new',
            method: "POST",
            headers: {
                authorization: statics.API_AUTH,
                void: req.siteData.userInfo.token
            },
            form: {
                game_id: req.body.game_id,
                mode_id: req.body.mode_id,
                prize: prize,
                server_id: req.body.server_id,
                description: req.body.note,
                console_id: req.body.console_id
            }
        };
        request(options, function (error, response, body) {

            if (!error && response.statusCode == 200) {
                const result = JSON.parse(body);
                // if (!result.error) {
                    res.json({ error: false, msg: result.msg })
                // }
            }
        });
    // } else {
    //     res.json({ error: true, msg: '569' })
    // }
});

router.post('/add-game-username',checkUser.checkUserAuth(), function (req, res, next) {
    const options = {
        url: '' + statics.API_URL + '/challenge/add-game-username',
        method: "POST",
        headers: {
            authorization: statics.API_AUTH,
            void: req.siteData.userInfo.token
        },
        form: {
            game_id: req.body.game_id,
            userName: req.body.userName
        }
    };
    request(options, function (error, response, body) {
        if (!error && response.statusCode == 200) {
            const result = JSON.parse(body);
            if (!result.error) {
                res.json({ error: false, msg: result.msg })
            }
        }
    });
});

router.get('/:id', function (req, res, next) {
    const options = {
        url: '' + statics.API_URL + '/challenge/get-one-challenge',
        method: "POST",
        headers: {
            authorization: statics.API_AUTH
        },
        form: {
            challenge_id: req.params.id
        }
    };
    if(typeof req.siteData.userInfo !== "undefined"){
        options.headers.void = req.siteData.userInfo.token
    }
    request(options, function (error, response, body) {
        if (!error && response.statusCode == 200) {
            const result = JSON.parse(body);
            if (!result.error) {
                res.render('challenge/this_challenge',
                    {
                        siteData: req.siteData,
                        data: result.data[0],
                        joining : result.joining
                    });
            }
        }
    });

});

module.exports = router;
