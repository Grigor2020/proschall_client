var express = require('express');
var router = express.Router();
var statics = require('../../static');
var request = require('request');
var checkUser = require('../../object/auth/auth');

router.post('/set-view',checkUser.checkUserAuth(), function (req, res, next) {
    let noteId = req.body.notifyId;
    const options = {
        url: '' + statics.API_URL + '/notification/viewed',
        method: "POST",
        headers: {
            authorization: statics.API_AUTH,
            void: req.siteData.userInfo.token
        },
        form: {
            notificationId: noteId
        }
    };
    request(options, function (error, response, body) {
        // console.log('error', error)
        if (!error && response.statusCode == 200) {
            const result = JSON.parse(body);
            if (!result.error) {
                res.json({ error: false, msg: result.msg })
            }
        }
    });
});

router.post('/get-all',checkUser.checkUserAuth(), function (req, res, next) {
    const options = {
        url: '' + statics.API_URL + '/notification/get-all',
        method: "POST",
        headers: {
            authorization: statics.API_AUTH,
            void: req.siteData.userInfo.token
        },
        form: {}
    };
    request(options, function (error, response, body) {
        // console.log('error', error)
        if (!error && response.statusCode == 200) {
            const result = JSON.parse(body);
            // console.log('111111111111result',result)
            if (!result.error) {
                res.json(result)
            }
        }
    });
});

module.exports = router;
