var express = require('express');
var router = express.Router();


/* GET home page. */
router.get('/', function (req, res, next) {
    // console.log('req.siteData',req.siteData)
    res.render('terms/terms',
        {
            siteData: req.siteData
        });
});

module.exports = router
