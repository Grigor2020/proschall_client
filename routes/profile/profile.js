var express = require('express');
var router = express.Router();
var statics = require('../../static');
const request = require('request');
var checkUser = require('../../object/auth/auth');

/* GET home page. */

router.get('/email-verification',function (req,res,next){
    const vToken = req.query.t;
    const options = {
        url: `${statics.API_URL}/profile/e_verify/accept`,
        method : "POST",
        headers: {
            'authorization': statics.API_AUTH,
            'void' :  req.siteData.userInfo.token
        },
        form: {
            token : vToken
        }
    };
    request(options, function (error, response, body) {
        const info = JSON.parse(body);
        if(!info.error){
            res.render('profile/detail',
                {
                    siteData: req.siteData
                });
        }else{
            res.render('default/not_found',
                {
                    siteData: req.siteData
                });
        }
    });

})

router.get('/deposit',checkUser.checkUserAuth(), function(req, res, next) {
    res.render('profile/deposit', {
        siteData: req.siteData
    })
});
router.get('/account-security',checkUser.checkUserAuth(), function(req, res, next) {
    res.render('profile/accountSecurity', {
        siteData: req.siteData
    })
});

router.get('/',checkUser.checkUserAuth(), function (req, res, next) {
    const options = {
        url: `${statics.API_URL}/profile/get-info`,
        headers: {
            'authorization': statics.API_AUTH,
            'void' : req.siteData.userInfo.token
        }
    };
    request(options, function (error, response, body) {
        const info = JSON.parse(body);
        if(!info.error){

            // console.log('asdasd',info.data)
            var gameUsernames = info.data.gameUsernames;
            var gameResults = info.data.gameResults;
            for(var i = 0; i < req.siteData.allGames.length; i++){
                for(var j = 0; j < gameUsernames.length; j++){
                    if(req.siteData.allGames[i].id === gameUsernames[j].game_id){
                        req.siteData.allGames[i].game_username = gameUsernames[j].name
                    }
                }
            }
            console.log('asdasd',gameResults)
            res.render('profile/profile',
                {
                    siteData: req.siteData,
                    gameResults : gameResults
                });
        }
    });
});
router.get('/detail',checkUser.checkUserAuth(), function (req, res, next) {
    console.log(req.siteData.userInfo)
    res.render('profile/detail',
        {
            siteData: req.siteData
        });
});

router.post('/deposit/c2c-deposit',checkUser.checkUserAuth(), function (req, res, next) {
    if(typeof req.body.cardNumber !== "undefined" && typeof req.body.amount !== "undefined"){
        const options = {
            url: ''+statics.API_URL+'/profile/deposit-add/c-2-c',
            method : "POST",
            headers: {
                'authorization': statics.API_AUTH,
                'void' :  req.siteData.userInfo.token
            },
            form: {
                card_number : req.body.cardNumber,
                amount : req.body.amount
            }
        };
        request(options, function (error, response, body) {
            if (!error && response.statusCode == 200) {
                const result = JSON.parse(body);
                if(!result.error){
                    res.redirect('/profile');
                }else {
                    res.json({error:true,msg:msg})
                }
            }
        });
    }else{
        res.json({error:true})
    }
});

router.post('/withdraw',checkUser.checkUserAuth(), function (req, res, next) {
    if(typeof req.body.bankName !== "undefined" && typeof req.body.bankAccountNumber !== "undefined" && typeof req.body.withdrawCardNumber !== "undefined" && typeof req.body.withdrawAmount !== "undefined"){
        const options = {
            url: ''+statics.API_URL+'/profile/withdraw',
            method : "POST",
            headers: {
                'authorization': statics.API_AUTH,
                'void' :  req.siteData.userInfo.token
            },
            form: {
                bank_name : req.body.bankName,
                bank_account_number : req.body.bankAccountNumber,
                card_number : req.body.withdrawCardNumber,
                amount : req.body.withdrawAmount
            }
        };
        request(options, function (error, response, body) {
            if (!error && response.statusCode == 200) {
                const result = JSON.parse(body);
                if(!result.error){
                    res.redirect('/profile');
                }else {
                    res.redirect('/profile/deposit#withdraw');
                }
            }
        });
    }else{
        res.json({error:true})
    }
});

router.post('/deposit/bitcoin-deposit',checkUser.checkUserAuth(), function (req, res, next) {
    if(typeof req.body.bitAmount !== "undefined" && typeof req.body.walletAddress !== "undefined"){
        const options = {
            url: ''+statics.API_URL+'/profile/deposit-add/bitcoin',
            method : "POST",
            headers: {
                'authorization': statics.API_AUTH,
                'void' :  req.siteData.userInfo.token
            },
            form: {
                wallet_address : req.body.walletAddress,
                amount : req.body.bitAmount
            }
        };
        request(options, function (error, response, body) {
            if (!error && response.statusCode == 200) {
                const result = JSON.parse(body);
                if(!result.error){
                    res.redirect('/profile');
                }else {
                    res.json({error:true,msg:msg})
                }
            }
        });
    }else{
        res.json({error:true})
    }
});

router.post('/deposit/pm-deposit',checkUser.checkUserAuth(), function (req, res, next) {
    console.log('c2c-deposit',req.body)
    if(typeof req.body.eVoucher !== "undefined" && typeof req.body.activationCode !== "undefined"){
        const options = {
            url: ''+statics.API_URL+'/profile/deposit-add/perfect-money',
            method : "POST",
            headers: {
                'authorization': statics.API_AUTH,
                'void' :  req.siteData.userInfo.token
            },
            form: {
                e_voucher : req.body.eVoucher,
                activation_code : req.body.activationCode
            }
        };
        request(options, function (error, response, body) {
            if (!error && response.statusCode == 200) {
                const result = JSON.parse(body);
                if(!result.error){
                    res.redirect('/profile');
                }else {
                    res.json({error:true,msg:msg})
                }
            }
        });
    }else{
        res.json({error:true})
    }
});

router.post('/update-bio',checkUser.checkUserAuth(), function (req, res, next) {
    if(typeof req.body.bio !== "undefined"){
        const options = {
            url: ''+statics.API_URL+'/profile/update-bio',
            method : "POST",
            headers: {
                'authorization': statics.API_AUTH,
                'void' :  req.siteData.userInfo.token
            },
            form: {
                bio : req.body.bio
            }
        };
        request(options, function (error, response, body) {
            if (!error && response.statusCode == 200) {
                const result = JSON.parse(body);
                if(!result.error){
                    res.redirect('/profile');
                }else {
                    res.redirect('/profile');
                }
            }
        });
    }else{
        res.json({error:true})
    }
});
router.post('/verify-email',checkUser.checkUserAuth(), function (req, res, next) {
    const options = {
        url: ''+statics.API_URL+'/profile/email-verify',
        method : "POST",
        headers: {
            'authorization': statics.API_AUTH,
            'void' :  req.siteData.userInfo.token
        },
        form: {}
    };
    request(options, function (error, response, body) {
        if (!error && response.statusCode == 200) {
            res.json({error:false})
        }
    });
});

router.post('/change-password',checkUser.checkUserAuth(), function (req, res, next) {
    const oldPassword = req.body.oldPassword;
    const newPassword = req.body.newPassword;
    const options = {
        url: ''+statics.API_URL+'/profile/change-password',
        method : "POST",
        headers: {
            'authorization': statics.API_AUTH,
            'void' :  req.siteData.userInfo.token
        },
        form: { oldPassword,newPassword }
    };
    request(options, function (error, response, body) {
        if (!error && response.statusCode == 200) {
            const result = JSON.parse(body);
            if(!result.error){
                res.json({error:false})
            }else {
                res.json({error:true})
            }
        }
    });
});

module.exports = router;
