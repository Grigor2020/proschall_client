var express = require('express');
var router = express.Router();
var statics = require('../../static');
const request = require('request');
var ip = require('ip');
var satelize = require('satelize');



router.get('/',function (req,res,next) {
    // satelize.satelize({ip:ip.address()}, function(err, payload) {
    //     console.log('.........',payload);
        const options = {
            url: ''+statics.API_URL+'/tournament/all',
            method : "POST",
            headers: {
                'authorization': statics.API_AUTH
            }
        };
        request(options, function (error, response, body) {
            if (!error && response.statusCode == 200) {
                const result = JSON.parse(body);
                if(!result.error){
                    console.log('result',result)
                    res.render('tournament/tournament',
                        {
                            siteData: req.siteData,
                            tournaments : result.data
                        })
                }else {
                    res.redirect('/')
                }
            }
        });
    // });

});

router.get('/item/:tournament_id',function (req,res,next) {
    // console.log('tournament_id',req.params.tournament_id)

    // satelize.satelize({ip:ip.address()}, function(err, payload) {
    //     const options = {
    //         url: ''+statics.API_URL+'/tournament/get-one-tournament',
    //         method : "POST",
    //         headers: {
    //             'authorization': statics.API_AUTH
    //         },
    //         form: {
    //             tournament_id : req.params.tournament_id
    //         }
    //     };
    //     request(options, function (error, response, body) {
    //         if (!error && response.statusCode == 200) {
    //             const result = JSON.parse(body);
    //             // console.log('result',result)
    //             // console.log('req.usesadsa',req.siteData.userInfo)
    //             if(!result.error){
    //                 let isRegistered = false;
    //                 if(typeof req.siteData.userInfo !== "undefined"){
    //                     if(result.data.tournamentMembers.indexOf(req.siteData.userInfo.id) !== -1){
    //                         isRegistered = true
    //                     }
    //                 }
    //
    //                 console.log('result.data.data',result.data.data.quarterFinals)
                    res.render('tournament/one_tournament',
                        {
                            siteData: req.siteData,
    //                         tournament : result.data.tournament,
    //                         tournamentMembers : result.data.regUsers,
    //                         data : result.data.data,
    //                         isRegistered : isRegistered,
    //                         type : typeof result.data.type !== "undefined" ? result.data.type : 0
                        })
    //             }else {
    //                 res.render('default/not_found',
    //                     {
    //                         siteData: req.siteData
    //                     })
    //             }
    //         }
    //     });
    // });

});


router.post('/get-tournament',function (req,res,next) {
    const options = {
        url: ''+statics.API_URL+'/tournament/get-one-tournament',
        method : "POST",
        headers: {
            'authorization': statics.API_AUTH
        },
        form: {
            tournament_id : req.body.tid
        }
    };
    request(options, function (error, response, body) {
        if (!error && response.statusCode == 200) {
            const result = JSON.parse(body);
            console.log('result',result)
            if(!result.error){
                let isRegistered = false;
                if(typeof req.siteData.userInfo !== "undefined"){
                    result.data.tournamentMembers.forEach((member)=>{
                        if(member.id == req.siteData.userInfo.id)
                            isRegistered = true
                    })
                }

                // console.log('result.data.data',result.data.data.quarterFinals)
                res.json({error : false,data : {
                        tournament: result.data.tournament,
                        tournamentMembers: result.data.tournamentMembers,
                        roundType: result.data.roundType,
                        actionType: result.data.actionType,
                        data: result.data.data,
                        isRegistered: isRegistered,
                        type: typeof result.data.type !== "undefined" ? result.data.type : 0
                    }})
            }else {
                res.render('default/not_found',
                    {
                        siteData: req.siteData
                    })
            }
        }
    });
});
router.post('/join',function (req,res,next) {

    // satelize.satelize({ip:ip.address()}, function(err, payload) {
        const options = {
            url: ''+statics.API_URL+'/tournament/user/join',
            method : "POST",
            headers: {
                'authorization': statics.API_AUTH
            },
            form: {
                tournament_id : req.body.tournament_id
            }
        };
    if(typeof req.siteData.userInfo !== "undefined"){
        options.headers.void = req.siteData.userInfo.token
        console.log('options',options)
        request(options, function (error, response, body) {
            if (!error && response.statusCode == 200) {
                const result = JSON.parse(body);
                console.log('result',result)
                res.json(result)
            }
        });
    }else{
        res.json({error:true,msg : '45695'})
    }

    // });

});

module.exports = router;
