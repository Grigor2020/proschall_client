var express = require('express');
var router = express.Router();
const request = require('request');
var static = require('../../static');
var checkUser = require('../../object/auth/auth');

/* GET users listing. */
router.post('/check-data', function(req, res, next) {
    if(req.body.username.length > 0 && req.body.email.length > 0 ){
        let username = req.body.username;
        let email = req.body.email;
        const formData = {
            "a_username" :  req.body.username,
            "a_email" :  req.body.email
        };
        request.post(
            {
                url: static.API_URL+'/auth/check-info',
                headers: {
                    'Accept': 'application/json',
                    'authorization' : static.API_AUTH
                },
                form: formData
            }, function (err, httpResponse, body) {
                var jsonBody = JSON.parse(body);
                if(!jsonBody.error){
                    res.json({error:false})
                }else{
                    res.json({error:true, msg : jsonBody.params})
                }
            }
        )
    }else{
        res.json({error:true,msg:'missing data'})
    }
});
router.post('/sign-up', function(req, res, next) {
    if ( req.body.firstName.length > 0 &&
        req.body.lastName.length > 0 &&
        req.body.username.length > 0 &&
        req.body.email.length > 0 &&
        req.body.phone.length > 0 &&
        req.body.password.length > 0 &&
        req.body.confirmPassword.length > 0 &&
        req.body.password === req.body.confirmPassword
    ){
        const formData = {
            "firstName" :req.body.firstName,
            "lastName" :  req.body.lastName,
            "username" :  req.body.username,
            "email" :  req.body.email,
            "phone" :  req.body.phone,
            "password" :  req.body.password,
            // "confirmPassword" :  req.body.confirmPassword,
        };
        console.log('formData',formData)
        request.post(
            {
                url: static.API_URL+'/auth/sign-up',
                headers: {
                    'Accept': 'application/json',
                    'authorization' : static.API_AUTH
                },
                form: formData
            }, function (err, httpResponse, body) {
                var jsonBody = JSON.parse(body);
                console.log('jsonBody',jsonBody)
                if(!jsonBody.error){
                    var token = jsonBody.user.token;
                    res.cookie('void', token,{expires: new Date(Date.now() + 900000000),path: '/'});
                    res.json({error:false})
                    res.end();
                }else{
                    res.json({error:true, errorCode : jsonBody.msg})
                    res.end();
                }
            }
        )
    }
});

router.post('/sign-in', function(req, res, next) {
    console.log('asdasdd',req.body)
    if (
        req.body.login.length > 0 &&
        req.body.password.length > 0
    )
    {
        const formData = {
            "a_login" :req.body.login,
            "a_password" :  req.body.password
        };
        request.post(
            {
                url: static.API_URL+'/auth/login',
                headers: {
                    'Accept': 'application/json',
                    'authorization' : static.API_AUTH
                },
                form: formData
            }, function (err, httpResponse, body) {
                var jsonBody = JSON.parse(body);
                if(!jsonBody.error){
                    res.cookie('void', jsonBody.user.token,{expires: new Date(Date.now() + 900000000),path: '/'});
                    res.json({error:false})
                    res.end();
                }else{
                    res.json({error:true, errorCode : jsonBody.msg})
                    res.end();
                }
                res.end();
            }
        )
    }
});

router.get('/logout', function(req, res, next) {
    res.clearCookie('void');
    res.redirect('/');
    res.end();

});

router.post('/get-messages',checkUser.checkUserAuth(), function(req, res, next) {
    request.post(
        {
            url: static.API_URL+'/auth/get-messages',
            headers: {
                'Accept': 'application/json',
                'authorization' : static.API_AUTH,
                'void' : req.siteData.userInfo.token
            },
            form:{
                matches_id : req.siteData.showMsg.matches_id
            }
        }, function (err, httpResponse, body) {
            var jsonBody = JSON.parse(body);
            if(!jsonBody.error){
                res.json({error:false, data : jsonBody.data})
            }else{
                res.json({error:true, errorCode : jsonBody.msg})

            }
            res.end();
        }
    )
});

router.post('/set-msg-viewed',checkUser.checkUserAuth(), function(req, res, next) {
    request.post(
        {
            url: static.API_URL+'/auth/set-messages-viewed',
            headers: {
                'Accept': 'application/json',
                'authorization' : static.API_AUTH,
                'void' : req.siteData.userInfo.token
            },
            form:{
                matches_id : req.siteData.showMsg.matches_id
            }
        }, function (err, httpResponse, body) {
            var jsonBody = JSON.parse(body);
            if(!jsonBody.error){
                res.json({error:false})
            }else{
                res.json({error:true})

            }
            res.end();
        }
    )
});

router.post('/send-msgs',checkUser.checkUserAuth(), function(req, res, next) {
    var objs = {
        url: static.API_URL+'/auth/set-messages',
        headers: {
            'Accept': 'application/json',
            'authorization' : static.API_AUTH,
            'void' : req.siteData.userInfo.token
        },
        form:{
            matches_id : req.siteData.showMsg.matches_id,
            message:req.body.msg
        }
    }
    console.log('objs',objs)
    request.post(
        objs, function (err, httpResponse, body) {
            var jsonBody = JSON.parse(body);
            if(!jsonBody.error){
                res.json({error:false})
            }else{
                res.json({error:true})

            }
            res.end();
        }
    )
});

module.exports = router;
