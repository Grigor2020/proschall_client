var express = require('express');
var router = express.Router();
var statics = require('../../static');
const request = require('request');
var checkUser = require('../../object/auth/auth');
var multer = require('multer');

var upload = multer({
    fileSize:1024 * 1024,
    fileFilter: (req, file, cb) => {
        if (file.mimetype == "image/png" || file.mimetype == "image/jpg" || file.mimetype == "image/jpeg" || file.mimetype == "video/mp4") {
            cb(null, true);
        } else {
            cb(null, false);
            return cb(new Error('Only .png, .jpg and .jpeg format allowed!'));
        }
    }//size of u file
});
const fs = require('fs');

upload.storage = multer.diskStorage({
    destination: '../api/public/images/dispute/',
    filename: function (req, file, cb) {
        let type = file.mimetype.split("/");
        let random = Math.round(Math.random()*1299999);
        let fullName = random + '-' + Date.now()+"."+type[1];
        cb(null, fullName);
    }
});

router.post('/join',checkUser.checkUserAuth(),function (req,res,next) {
    // console.log(req.siteData.userInfo);
    if(typeof req.body.challengeId !== "undefined"){
        const options = {
            url: ''+statics.API_URL+'/match/join',
            method : "POST",
            headers: {
                'authorization': statics.API_AUTH,
                'void' : req.siteData.userInfo.token
            },
            form: {
                challengeId : req.body.challengeId
            }
        };
        request(options, function (error, response, body) {
            if (!error && response.statusCode == 200) {
                const result = JSON.parse(body);
                if(!result.error){
                    res.json({error:false,msg:result.msg});
                }else {
                    res.json(result)
                }
            }
        });
    }else{
        res.json({error:true})
    }

})

var cpUpload = upload.fields([
    { name: 'disputeFile', maxCount: 1 }]);

router.post('/dispute',cpUpload ,checkUser.checkUserAuth(), function (req, res, next) {
    console.log('cpUpload',cpUpload)
    console.log('req.body',req.body)
    console.log('req.files',req.files)
    const options = {
        url: ''+statics.API_URL+'/match/dispute-add',
        method : "POST",
        headers: {
            'authorization': statics.API_AUTH,
            'void' :  req.siteData.userInfo.token
        },
        form: {
            message : req.body.disputeGameResult,
            description : req.body.disputeMessage,
            fileName : req.files.disputeFile[0].filename,
            matchesId: req.body.mId
        }
    };

    request(options, function (error, response, body) {
        if (!error && response.statusCode == 200) {
            const result = JSON.parse(body);
            console.log('result',result)
            if(!result.error){
                res.redirect('/profile');
            }else {
                res.json({error:true,msg:msg})
            }
        }
    });
});

router.post('/set-accept',checkUser.checkUserAuth(),function (req,res,next) {
    // console.log(req.siteData.userInfo);
    if(typeof req.body.matchId !== "undefined" && typeof req.body.accept !== "undefined" ){
        const options = {
            url: ''+statics.API_URL+'/match/accept',
            method : "POST",
            headers: {
                'authorization': statics.API_AUTH,
                'void' : req.siteData.userInfo.token
            },
            form: {
                matchesId : req.body.matchId
            }
        };
        request(options, function (error, response, body) {
            if (!error && response.statusCode == 200) {
                const result = JSON.parse(body);
                if(!result.error){
                    res.json({error:false});
                }else {
                    res.json(result)
                }
            }
        });
    }else{
        res.json({error:true})
    }

})

router.post('/set-start',checkUser.checkUserAuth(),function (req,res,next) {
    const options = {
        url: statics.API_URL+'/match/start',
        method : "POST",
        headers: {
            'authorization': statics.API_AUTH,
            'void' : req.siteData.userInfo.token
        },
        form: {
            matchesId : req.body.matchId
        }
    };
    request(options, function (error, response, body) {
        if (!error && response.statusCode == 200) {
            const result = JSON.parse(body);
            if(!result.error){
                // console.log('result',result)
                res.json(result)
            }
        }
    });

})

router.post('/set-decline',checkUser.checkUserAuth(),function (req,res,next) {
    const options = {
        url: statics.API_URL+'/match/decline',
        method : "POST",
        headers: {
            'authorization': statics.API_AUTH,
            'void' : req.siteData.userInfo.token
        },
        form: {
            matchesId : req.body.matchId
        }
    };
    request(options, function (error, response, body) {
        if (!error && response.statusCode == 200) {
            const result = JSON.parse(body);
            if(!result.error){
                res.json(result)
            }
        }
    });
})

router.post('/situation',checkUser.checkUserAuth(),function (req,res,next) {
    const options = {
        url: statics.API_URL+'/match/set-result',
        method : "POST",
        headers: {
            'authorization': statics.API_AUTH,
            'void' : req.siteData.userInfo.token
        },
        form: {
            matchesId : req.body.matchId,
            result : req.body.gameSituation
        }
    };
    request(options, function (error, response, body) {
        if (!error && response.statusCode == 200) {
            const result = JSON.parse(body);
            // if(!result.error){
                res.json(result)
            // }
        }
    });
})

router.post('/get-my-history',checkUser.checkUserAuth(),function (req,res,next) {
    const options = {
        url: statics.API_URL+'/match/get-my-history',
        method : "POST",
        headers: {
            'authorization': statics.API_AUTH,
            'void' : req.siteData.userInfo.token
        }
    };
    request(options, function (error, response, body) {
        if (!error && response.statusCode == 200) {
            const result = JSON.parse(body);
            if(!result.error){
                console.log('result',result)
                res.json(result)
            }
        }
    });
})

router.post('/check-winner',checkUser.checkUserAuth(),function (req,res,next) {
    const options = {
        url: statics.API_URL+'/match/check-winner',
        method : "POST",
        headers: {
            'authorization': statics.API_AUTH,
            'void' : req.siteData.userInfo.token
        },
        form: {
            matchesId : req.body.matchId
        }
    };
    request(options, function (error, response, body) {
        if (!error && response.statusCode == 200) {
            const result = JSON.parse(body);
            if(!result.error){
                res.json(result)
            }
        }
    });
})


router.get('/:id',checkUser.checkUserAuth(),function (req,res,next) {
    if(typeof req.params.id !== "undefined"){
        const options = {
            url: ''+statics.API_URL+'/match/get-this',
            method : "POST",
            headers: {
                'authorization': statics.API_AUTH,
                'void' : req.siteData.userInfo.token
            },
            form: {
                matchesId : req.params.id
            }
        };
        request(options, function (error, response, body) {
            if (!error && response.statusCode == 200) {
                const result = JSON.parse(body);
                console.log('result',result)
                if(!result.error){
                    res.render('match/match',{
                        siteData: req.siteData,
                        data:result.data
                    })
                }else {
                    res.render('default/not_found',{
                        siteData: req.siteData
                    })
                }
            }
        });
    }else{
        res.json({error:true})
    }
});

module.exports = router;
