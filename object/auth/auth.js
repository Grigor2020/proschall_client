var statics = require('../../static');


const checkUserAuth = function (req, res, next) {
    return function(req, res, next) {
        if(typeof req.siteData.userInfo !== "undefined"){
            next();
        }else{
            res.render('default/not_found',{siteData :req.siteData});
        }

    }
}
module.exports.checkUserAuth = checkUserAuth;
